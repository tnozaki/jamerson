/*-
 * Copyright (c) 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */
#ifndef T_JAMERSON_DEFS_H
#define T_JAMERSON_DEFS_H

#include "jamerson_defs.h"

#if HAVE_ATF_C_H
#include <atf-c.h>
#else
#include <assert.h>
#include <stdio.h>
#define ATF_TC(arg0)		static void arg0##_head(void)
#define ATF_TC_HEAD(arg0, arg1)	static void arg0##_head()
#define atf_tc_set_md_var(arg0, arg1, ...) do {			\
	printf(__VA_ARGS__);					\
	puts("");						\
} while (/*CONSTCOND*/0)
#define ATF_TC_BODY(arg0, arg1)	static void arg0##_body()
#define ATF_CHECK(arg0)		assert(arg0)
#define ATF_TP_ADD_TCS(arg0)	static int test(void)
#define ATF_TP_ADD_TC(arg0, arg1) do {				\
	arg1##_head();						\
	arg1##_body();						\
} while (/*CONSTCOND*/0)
#define atf_no_error()		0
#include <stdlib.h>
static int test(void);
int
main(int argc, const char *argv[])
{
	setprogname(argv[0]);
	return test();
}
#endif

#endif
