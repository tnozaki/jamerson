/*-
 * Copyright (c) 2018, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_jamerson_defs.h"

#include <limits.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "byte_array_builder.h"

ATF_TC(test_byte_array);
ATF_TC_HEAD(test_byte_array, tc)
{
	atf_tc_set_md_var(tc, "descr", "test_byte_array");
}
ATF_TC_BODY(test_byte_array, tc)
{
	byte_array_builder_t bab;
	size_t i, n;
	char tests[BUFSIZ], *ba;
	int c;
	FILE *fp;

	for (i = 0; i < arraycount(tests); ++i)
		tests[i] = (unsigned char)arc4random_uniform(0x100);
	bab = byte_array_builder_new(&ba, &n);
	for (i = 0; i < arraycount(tests); ++i) {
		c = (unsigned char)tests[i];
		ATF_CHECK(byte_array_builder_append(bab, c) == c);
	}
	ATF_CHECK(byte_array_builder_shrink_to_fit(bab) == 0);
	ATF_CHECK(n == arraycount(tests));
	byte_array_builder_delete(bab);
	for (i = 0; i < n; ++i)
		ATF_CHECK(ba[i] == tests[i]);
	ATF_CHECK(ba[n] == '\0');
	free(ba);
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_byte_array);

	return atf_no_error();
}
