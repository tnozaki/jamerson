/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include "t_jamerson_defs.h"

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <math.h>

#include "jamerson_load.h"
#include "jamerson_store.h"
#include "jamerson_local.h"

static const bool boolean_tests[] = {
	true,
	false
};
static const double number_tests[] = {
	1,
	2,
	3,
};
static const char *string_tests[] = {
	"test1",
	"test2",
	"test3"
};

#define FOREACH_TEST(name, type)				\
static void							\
foreach_test_##name(void *ctx, void (*func)(void *, type))	\
{								\
	size_t i;						\
	for (i = 0; i < arraycount(name##_tests); ++i)		\
		(*func)(ctx, name##_tests[i]); 			\
}
FOREACH_TEST(boolean, bool)
FOREACH_TEST(number, double)
FOREACH_TEST(string, const char *)

static void
test_boolean_compare(jamerson_boolean_t boolean_value,
    bool boolean_test)
{
	ATF_CHECK(jamerson_boolean_boolean_value(boolean_value)
	    == boolean_test);
}
static void
test_number_compare(jamerson_number_t number_value,
    double number_test)
{
	ATF_CHECK(jamerson_number_double_value(number_value)
	    == number_test);
}
static void
test_string_compare(jamerson_string_t string_value,
    const char *string_test)
{
	const char *real_value;

	real_value = jamerson_string_string_value(string_value);
	ATF_CHECK(real_value != NULL);
	ATF_CHECK(!strcmp(real_value, string_test));
}

static void
test_array_check(jamerson_value_t value)
{
	jamerson_array_t array_value;

	ATF_CHECK(jamerson_value_is_boolean(value) == NULL);
	ATF_CHECK(jamerson_value_is_null(value)    == NULL);
	ATF_CHECK(jamerson_value_is_number(value)  == NULL);
	ATF_CHECK(jamerson_value_is_object(value)  == NULL);
	ATF_CHECK(jamerson_value_is_string(value)  == NULL);
	array_value = jamerson_value_is_array(value);
	ATF_CHECK(array_value != NULL);
}
static void
test_boolean_check(jamerson_value_t value, bool boolean_test)
{
	jamerson_boolean_t boolean_value;

	ATF_CHECK(jamerson_value_is_array(value)  == NULL);
	ATF_CHECK(jamerson_value_is_null(value)   == NULL);
	ATF_CHECK(jamerson_value_is_number(value) == NULL);
	ATF_CHECK(jamerson_value_is_object(value) == NULL);
	ATF_CHECK(jamerson_value_is_string(value) == NULL);
	boolean_value = jamerson_value_is_boolean(value);
	ATF_CHECK(boolean_value != NULL);
	test_boolean_compare(boolean_value, boolean_test);
}
static void
test_null_check(jamerson_value_t value)
{
	jamerson_null_t null_value;

	ATF_CHECK(jamerson_value_is_array(value)   == NULL);
	ATF_CHECK(jamerson_value_is_boolean(value) == NULL);
	ATF_CHECK(jamerson_value_is_number(value)  == NULL);
	ATF_CHECK(jamerson_value_is_object(value)  == NULL);
	ATF_CHECK(jamerson_value_is_string(value)  == NULL);
	null_value = jamerson_value_is_null(value);
	ATF_CHECK(null_value != NULL);
}
static void
test_number_check(jamerson_value_t value, double number_test)
{
	jamerson_number_t number_value;

	ATF_CHECK(jamerson_value_is_array(value)   == NULL);
	ATF_CHECK(jamerson_value_is_boolean(value) == NULL);
	ATF_CHECK(jamerson_value_is_null(value)    == NULL);
	ATF_CHECK(jamerson_value_is_object(value)  == NULL);
	ATF_CHECK(jamerson_value_is_string(value)  == NULL);
	number_value = jamerson_value_is_number(value);
	ATF_CHECK(number_value != NULL);
	test_number_compare(number_value, number_test);
}
static void
test_string_check(jamerson_value_t value, const char *string_test)
{
	jamerson_string_t string_value;

	ATF_CHECK(jamerson_value_is_array(value)   == NULL);
	ATF_CHECK(jamerson_value_is_boolean(value) == NULL);
	ATF_CHECK(jamerson_value_is_null(value)    == NULL);
	ATF_CHECK(jamerson_value_is_number(value)  == NULL);
	ATF_CHECK(jamerson_value_is_object(value)  == NULL);
	string_value = jamerson_value_is_string(value);
	ATF_CHECK(string_value != NULL);
	test_string_compare(string_value, string_test);
}
static void
test_object_check(jamerson_value_t value)
{
	jamerson_object_t object_value;

	ATF_CHECK(jamerson_value_is_array(value)   == NULL);
	ATF_CHECK(jamerson_value_is_boolean(value) == NULL);
	ATF_CHECK(jamerson_value_is_null(value)    == NULL);
	ATF_CHECK(jamerson_value_is_number(value)  == NULL);
	ATF_CHECK(jamerson_value_is_string(value)  == NULL);
	object_value = jamerson_value_is_object(value);
	ATF_CHECK(object_value != NULL);
}

#define DO_TEST(name, type, attr)				\
static void							\
do_test_##name(void *ctx, type name##_test)			\
{								\
	jamerson_##name##_t value;				\
	jamerson_value_t wrapper;				\
								\
	value = jamerson_##name##_new(name##_test);		\
	ATF_CHECK(value != NULL);				\
	test_##name##_compare(value, name##_test);		\
	jamerson_##name##_delete(value);			\
								\
	value = jamerson_##name##_new(name##_test);		\
	wrapper = jamerson_value_new_##name(value);		\
	ATF_CHECK(wrapper != NULL);				\
	test_##name##_check(wrapper, name##_test);		\
	jamerson_value_delete(wrapper);				\
}
DO_TEST(boolean, bool, boolean_value)
DO_TEST(number, double, double_value)
DO_TEST(string, const char *, string_value)

#define TEST_SIMPLE(name)					\
ATF_TC(test_##name);						\
ATF_TC_HEAD(test_##name, tc)					\
{								\
	atf_tc_set_md_var(tc, "descr",				\
	    "test jamerson_" #name "_t");			\
}								\
ATF_TC_BODY(test_##name, tc)					\
{								\
	jamerson_##name##_t value;				\
	jamerson_value_t wrapper;				\
								\
	value = jamerson_##name##_new();			\
	ATF_CHECK(value != NULL);				\
	jamerson_##name##_delete(value);			\
								\
	value = jamerson_##name##_new();			\
	wrapper = jamerson_value_new_##name(value);		\
	ATF_CHECK(wrapper != NULL);				\
	test_##name##_check(wrapper);				\
	jamerson_value_delete(wrapper);				\
}
TEST_SIMPLE(array)
TEST_SIMPLE(null)
TEST_SIMPLE(object)

#define TEST_FOREACH(name)					\
ATF_TC(test_##name);						\
ATF_TC_HEAD(test_##name, tc)					\
{								\
	atf_tc_set_md_var(tc, "descr",				\
	    "test jamerson_" #name "_t");			\
}								\
ATF_TC_BODY(test_##name, tc)					\
{								\
	foreach_test_##name(NULL, &do_test_##name);		\
}
TEST_FOREACH(boolean)
TEST_FOREACH(number)
TEST_FOREACH(string)

#define TEST_COMPLEX_CTX(name)					\
struct test_##name##_ctx {					\
	jamerson_value_t parent;				\
	jamerson_##name##_t name;				\
	size_t idx;						\
}
TEST_COMPLEX_CTX(array);
TEST_COMPLEX_CTX(object);

#define TEST_ARRAY_ADD0(name)					\
static void							\
test_array_##name##_add(void * ctx)				\
{								\
	struct test_array_ctx *p;				\
	jamerson_##name##_t value;				\
	jamerson_value_t wrapper;				\
	int ret;						\
	size_t size;						\
								\
	p = (struct test_array_ctx *)ctx;			\
	value = jamerson_##name##_new();			\
	ATF_CHECK(value != NULL);				\
	wrapper = jamerson_value_new_##name(value);		\
	ATF_CHECK(wrapper != NULL);				\
	jamerson_value_set_parent(wrapper, p->parent);		\
	ret = jamerson_array_push_back(p->array, wrapper);	\
	ATF_CHECK(ret == 0);					\
	size = jamerson_array_size(p->array);			\
	ATF_CHECK(size == ++p->idx);				\
}
TEST_ARRAY_ADD0(array)
TEST_ARRAY_ADD0(null)
TEST_ARRAY_ADD0(object)

#define TEST_ARRAY_ADD1(name, type)				\
static void							\
test_array_##name##_add(void * ctx, type name##_value)		\
{								\
	struct test_array_ctx *p;				\
	jamerson_##name##_t value;				\
	jamerson_value_t wrapper;				\
	int ret;						\
	size_t size;						\
								\
	p = (struct test_array_ctx *)ctx;			\
	value = jamerson_##name##_new(name##_value);		\
	ATF_CHECK(value != NULL);				\
	wrapper = jamerson_value_new_##name(value);		\
	ATF_CHECK(wrapper != NULL);				\
	jamerson_value_set_parent(wrapper, p->parent);		\
	ret = jamerson_array_push_back(p->array, wrapper);	\
	ATF_CHECK(ret == 0);					\
	size = jamerson_array_size(p->array);			\
	ATF_CHECK(size == ++p->idx);				\
}
TEST_ARRAY_ADD1(boolean, bool)
TEST_ARRAY_ADD1(number, double)
TEST_ARRAY_ADD1(string, const char *)

#define TEST_ARRAY_CHECK0(name)					\
static void							\
test_array_##name##_check(void *ctx)				\
{								\
	struct test_array_ctx *p;				\
	jamerson_value_t value;					\
								\
	p = (struct test_array_ctx *)ctx;			\
	value = jamerson_array_get(p->array, p->idx++);		\
	ATF_CHECK(value != NULL);				\
	ATF_CHECK(jamerson_value_get_parent(value)		\
	    == p->parent);					\
	test_##name##_check(value);				\
}
TEST_ARRAY_CHECK0(array)
TEST_ARRAY_CHECK0(null)
TEST_ARRAY_CHECK0(object)

#define TEST_ARRAY_CHECK1(name, type)				\
static void							\
test_array_##name##_check(void *ctx, type name##_value)		\
{								\
	struct test_array_ctx *p;				\
	jamerson_value_t value;					\
								\
	p = (struct test_array_ctx *)ctx;			\
	value = jamerson_array_get(p->array, p->idx++);		\
	ATF_CHECK(value != NULL);				\
	ATF_CHECK(jamerson_value_get_parent(value)		\
	    == p->parent);					\
	test_##name##_check(value, name##_value);		\
}
TEST_ARRAY_CHECK1(boolean, bool)
TEST_ARRAY_CHECK1(number, double)
TEST_ARRAY_CHECK1(string, const char *)

#define TEST_OBJECT_ADD0(name)					\
static void							\
test_object_##name##_add(void * ctx)				\
{								\
	struct test_object_ctx *p;				\
	jamerson_##name##_t value;				\
	jamerson_value_t wrapper, old;				\
	char *key;						\
	int ret;						\
								\
	p = (struct test_object_ctx *)ctx;			\
	value = jamerson_##name##_new();			\
	ATF_CHECK(value != NULL);				\
	wrapper = jamerson_value_new_##name(value);		\
	ATF_CHECK(wrapper != NULL);				\
	jamerson_value_set_parent(wrapper, p->parent);		\
	asprintf(&key, #name "_%zd", p->idx++);			\
	ATF_CHECK(key != NULL);					\
	ret = jamerson_object_put(p->object,			\
	    key, wrapper, &old);				\
	free(key);						\
	ATF_CHECK(ret == 0);					\
	ATF_CHECK(old == NULL);					\
}
TEST_OBJECT_ADD0(array)
TEST_OBJECT_ADD0(null)
TEST_OBJECT_ADD0(object)

#define TEST_OBJECT_ADD1(name, type)				\
static void							\
test_object_##name##_add(void * ctx, type name##_value)		\
{								\
	struct test_object_ctx *p;				\
	jamerson_##name##_t value;				\
	jamerson_value_t wrapper, old;				\
	char *key;						\
	int ret;						\
								\
	p = (struct test_object_ctx *)ctx;			\
	value = jamerson_##name##_new(name##_value);		\
	ATF_CHECK(value != NULL);				\
	wrapper = jamerson_value_new_##name(value);		\
	ATF_CHECK(wrapper != NULL);				\
	jamerson_value_set_parent(wrapper, p->parent);		\
	asprintf(&key, #name "_%zd", p->idx++);			\
	ATF_CHECK(key != NULL);					\
	ret = jamerson_object_put(p->object,			\
	    key, wrapper, &old);				\
	free(key);						\
	ATF_CHECK(ret == 0);					\
	ATF_CHECK(old == NULL);					\
}
TEST_OBJECT_ADD1(boolean, bool)
TEST_OBJECT_ADD1(number, double)
TEST_OBJECT_ADD1(string, const char *)

#define TEST_OBJECT_CHECK0(name)				\
static void							\
test_object_##name##_check(void *ctx)				\
{								\
	struct test_object_ctx *p;				\
	jamerson_value_t value;					\
	char *key;						\
								\
	p = (struct test_object_ctx *)ctx;			\
	asprintf(&key, #name "_%zd", p->idx++);			\
	ATF_CHECK(key != NULL);					\
	value = jamerson_object_get(p->object, key);		\
	free(key);						\
	ATF_CHECK(value != NULL);				\
	ATF_CHECK(jamerson_value_get_parent(value)		\
	    == p->parent);					\
	test_##name##_check(value);				\
}
TEST_OBJECT_CHECK0(array)
TEST_OBJECT_CHECK0(null)
TEST_OBJECT_CHECK0(object)

#define TEST_OBJECT_CHECK1(name, type)				\
static void							\
test_object_##name##_check(void *ctx, type name##_value)	\
{								\
	struct test_object_ctx *p;				\
	jamerson_value_t value;					\
	char *key;						\
								\
	p = (struct test_object_ctx *)ctx;			\
	asprintf(&key, #name "_%zd", p->idx++);			\
	ATF_CHECK(key != NULL);					\
	value = jamerson_object_get(p->object, key);		\
	free(key);						\
	ATF_CHECK(value != NULL);				\
	ATF_CHECK(jamerson_value_get_parent(value)		\
	    == p->parent);					\
	test_##name##_check(value, name##_value);		\
}
TEST_OBJECT_CHECK1(boolean, bool)
TEST_OBJECT_CHECK1(number, double)
TEST_OBJECT_CHECK1(string, const char *)

#define TEST_COMPLEX(name)					\
ATF_TC(test_##name##_complex);					\
ATF_TC_HEAD(test_##name##_complex, tc)				\
{								\
	atf_tc_set_md_var(tc, "descr",				\
	    "test jamerson_" #name "_t(complex)");		\
}								\
ATF_TC_BODY(test_##name##_complex, tc)				\
{								\
	struct test_##name##_ctx ctx;				\
								\
	ctx.name = jamerson_##name##_new();			\
	ATF_CHECK(ctx.name != NULL);				\
	ctx.parent = jamerson_value_new_##name(ctx.name);	\
	ATF_CHECK(ctx.parent != NULL);				\
								\
	ctx.idx = 0;						\
	test_##name##_array_add(&ctx);				\
	foreach_test_boolean(&ctx,				\
	    &test_##name##_boolean_add);			\
	test_##name##_null_add(&ctx);				\
	foreach_test_number(&ctx,				\
	    &test_##name##_number_add);				\
	foreach_test_string(&ctx,				\
	    &test_##name##_string_add);				\
	test_##name##_object_add(&ctx);				\
								\
	ctx.idx = 0;						\
	test_##name##_array_check(&ctx);			\
	foreach_test_boolean(&ctx,				\
	    &test_##name##_boolean_check);			\
	test_##name##_null_check(&ctx);				\
	foreach_test_number(&ctx,				\
	    &test_##name##_number_check);			\
	foreach_test_string(&ctx,				\
	    &test_##name##_string_check);			\
	test_##name##_object_check(&ctx);			\
								\
	jamerson_value_delete(ctx.parent);			\
}
TEST_COMPLEX(array)
TEST_COMPLEX(object)

struct testcase {
	const char *var;
	size_t lenvar;
};
#define TESTCASE(str)	{ .var = str, .lenvar = sizeof(str) }

ATF_TC(test_parser_fail);
ATF_TC_HEAD(test_parser_fail, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    "test jamerson parser fail");
}
ATF_TC_BODY(test_parser_fail, tc)
{
	static const struct testcase parser_fail_test[] = {
		TESTCASE(""),
		TESTCASE(" "),
		TESTCASE("\f"),
		TESTCASE("\n"),
		TESTCASE("\r"),
		TESTCASE("\t"),
		TESTCASE("\v"),
		TESTCASE("t"),
		TESTCASE("tr"),
		TESTCASE("tru"),
		TESTCASE("truing"),
		TESTCASE("trued"),
		TESTCASE("true or false"),
		TESTCASE("n"),
		TESTCASE("nu"),
		TESTCASE("nul"),
		TESTCASE("nuld"),
		TESTCASE("nullify"),
		TESTCASE("null pointer exception"),
		TESTCASE("\""),
		TESTCASE("\"s"),
		TESTCASE("\"st"),
		TESTCASE("\"str"),
		TESTCASE("\"stri"),
		TESTCASE("\"strin"),
		TESTCASE("\"string"),
		TESTCASE("["),
		TESTCASE("]"),
		TESTCASE("{"),
		TESTCASE("}"),
		TESTCASE("[true"),
		TESTCASE("[false"),
		TESTCASE("[null"),
		TESTCASE("[1.0"),
		TESTCASE("[\"string\""),
		TESTCASE("\"key\":true"),
		TESTCASE("\"key\":false"),
		TESTCASE("\"key\":null"),
		TESTCASE("\"key\":1.0"),
		TESTCASE("\"key\":\"string\""),
		TESTCASE("{\"key\":true"),
		TESTCASE("{\"key\":false"),
		TESTCASE("{\"key\":null"),
		TESTCASE("{\"key\":1.0"),
		TESTCASE("{\"key\":\"string\""),
		TESTCASE("{true}"),
		TESTCASE("{false}"),
		TESTCASE("{null}"),
		TESTCASE("{1.0}"),
		TESTCASE("{\"string\"}"),
		TESTCASE("[\"key\":true]"),
		TESTCASE("[\"key\":false]"),
		TESTCASE("[\"key\":null]"),
		TESTCASE("[\"key\":1.0]"),
		TESTCASE("[\"key\":\"string\"]"),
	};
	const struct testcase *t;
	FILE *fp;
	jamerson_value_t root;
	size_t i;

	for (i = 0; i < arraycount(parser_fail_test); ++i) {
		t = &parser_fail_test[i];
		fp = fmemopen((void *)t->var, t->lenvar, "r");
		ATF_CHECK(fp != NULL);
		root = jamerson_load_file(fp);
		ATF_CHECK(root == NULL);
	}
}

ATF_TC(test_parser_success);
ATF_TC_HEAD(test_parser_success, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    "test jamerson parser success");
}
ATF_TC_BODY(test_parser_success, tc)
{
	static const struct testcase parser_success_test[] = {
		TESTCASE("true"),
		TESTCASE("1.0"),
		TESTCASE("null"),
		TESTCASE("\"string\""),
		TESTCASE(" true"),
		TESTCASE(" 1.0"),
		TESTCASE(" null"),
		TESTCASE(" \"string\""),
		TESTCASE("\ftrue"),
		TESTCASE("\f1.0"),
		TESTCASE("\fnull"),
		TESTCASE("\f\"string\""),
		TESTCASE("\ntrue"),
		TESTCASE("\n1.0"),
		TESTCASE("\nnull"),
		TESTCASE("\n\"string\""),
		TESTCASE("\rtrue"),
		TESTCASE("\r1.0"),
		TESTCASE("\rnull"),
		TESTCASE("\r\"string\""),
		TESTCASE("\vtrue"),
		TESTCASE("\v1.0"),
		TESTCASE("\vnull"),
		TESTCASE("\v\"string\""),
		TESTCASE("[]"),
		TESTCASE(" []"),
		TESTCASE("\f[]"),
		TESTCASE("\n[]"),
		TESTCASE("\r[]"),
		TESTCASE("\t[]"),
		TESTCASE("\v[]"),
		TESTCASE("[[]]"),
		TESTCASE("[[],[]]"),
		TESTCASE("[true]"),
		TESTCASE("[true,false]"),
		TESTCASE("[null]"),
		TESTCASE("[null,null]"),
		TESTCASE("[1]"),
		TESTCASE("[1,2]"),
		TESTCASE("[\"string\"]"),
		TESTCASE("[\"string\",\"string\"]"),
		TESTCASE("[{}]"),
		TESTCASE("[{},{}]"),
		TESTCASE("[[],true,null,1,\"string\",{}]"),
		TESTCASE("{}"),
		TESTCASE(" {}"),
		TESTCASE("\f{}"),
		TESTCASE("\n{}"),
		TESTCASE("\r{}"),
		TESTCASE("\t{}"),
		TESTCASE("\v{}"),
		TESTCASE("{\"key\":[]}"),
		TESTCASE("{\"key1\":[],\"key2\":[]}"),
		TESTCASE("{\"key\":true}"),
		TESTCASE("{\"key1\":true,\"key2\":false}"),
		TESTCASE("{\"key\":null}"),
		TESTCASE("{\"key1\":null,\"key2\":null}"),
		TESTCASE("{\"key\":1}"),
		TESTCASE("{\"key1\":1,\"key2\":2}"),
		TESTCASE("{\"key\":\"string\"}"),
		TESTCASE("{\"key1\":\"string\",\"key2\":\"string\"}"),
		TESTCASE("{\"key\":{}}"),
		TESTCASE("{\"key1\":{},\"key2\":{}}"),
	};
	const struct testcase *t;
	FILE *fp;
	jamerson_value_t root;
	size_t i;

	for (i = 0; i < arraycount(parser_success_test); ++i) {
		t = &parser_success_test[i];
		fp = fmemopen((void *)t->var, t->lenvar, "r");
		ATF_CHECK(fp != NULL);
		root = jamerson_load_file(fp);
		ATF_CHECK(root != NULL);
		jamerson_value_delete(root);
	}
}

ATF_TC(test_parser_string_fail);
ATF_TC_HEAD(test_parser_string_fail, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    "test jamerson parser stringu fail");
}
ATF_TC_BODY(test_parser_string_fail, tc)
{
	static const struct testcase parser_string_fail_testcases[] = {
		TESTCASE("\"\\\""),
		TESTCASE("\"\\u\""),
		TESTCASE("\"\\uG\""),
		TESTCASE("\"\\ug\""),
		TESTCASE("\"\\u0\""),
		TESTCASE("\"\\u00\""),
		TESTCASE("\"\\u000\""),
		TESTCASE("\"\\ud800\\ud7ff\""),
		TESTCASE("\"\\ud800\\ud800\""),
		TESTCASE("\"\\ud800\\udbff\""),
		TESTCASE("\"\\ud800\\ue000\""),
		TESTCASE("\"\\udc00\""),
		TESTCASE("\"\\udfff\""),
		TESTCASE("\"\\U007f\""),
	};
	const struct testcase *t;
	FILE *fp;
	jamerson_value_t root;
	size_t i;

	for (i = 0; i < arraycount(parser_string_fail_testcases); ++i) {
		t = &parser_string_fail_testcases[i];
		fp = fmemopen((void *)t->var, t->lenvar, "r");
		ATF_CHECK(fp != NULL);
		root = jamerson_load_file(fp);
		ATF_CHECK(root == NULL);
	}
}

struct stringtest {
	const char *var;
	size_t lenvar;
	const char *expected;
};
#define STRINGTEST(var, expected) { var, sizeof(var), expected }

ATF_TC(test_parser_string_success);
ATF_TC_HEAD(test_parser_string_success, tc)
{
	atf_tc_set_md_var(tc, "descr",
	    "test jamerson parser string success");
}
ATF_TC_BODY(test_parser_string_success, tc)
{
	static const struct stringtest parser_string_success_testcases[] = {
		STRINGTEST("\"\\\"\"", "\x22"),
		STRINGTEST("\"\\\\\"", "\x5c"),
		STRINGTEST("\"\\/\"", "\x2f"),
		STRINGTEST("\"\\b\"", "\x08"),
		STRINGTEST("\"\\f\"", "\x0c"),
		STRINGTEST("\"\\n\"", "\x0a"),
		STRINGTEST("\"\\r\"", "\x0d"),
		STRINGTEST("\"\\t\"", "\x09"),
		STRINGTEST("\"\\u007F\"", "\x7f"),
		STRINGTEST("\"\\u007f\"", "\x7f"),
		STRINGTEST("\"\\u0080\"", "\xc2\x80"),
		STRINGTEST("\"\\u07FF\"", "\xdf\xbf"),
		STRINGTEST("\"\\u07ff\"", "\xdf\xbf"),
		STRINGTEST("\"\\u0800\"", "\xe0\xa0\x80"),
		STRINGTEST("\"\\uFFFF\"", "\xef\xbf\xbf"),
		STRINGTEST("\"\\uffff\"", "\xef\xbf\xbf"),
		STRINGTEST("\"\\uD800\\uDC00\"", "\xf0\x90\x80\x80"),
		STRINGTEST("\"\\ud800\\udc00\"", "\xf0\x90\x80\x80"),
		STRINGTEST("\"\\uDBFF\\uDFFF\"", "\xf4\x8f\xbf\xbf"),
		STRINGTEST("\"\\udbff\\udfff\"", "\xf4\x8f\xbf\xbf"),
		STRINGTEST("\"\\u00300\"", "\x30\x30"),
		STRINGTEST("\"\\u00300\"", "\x30\x30"),
	};
	const struct stringtest *t;
	FILE *fp;
	jamerson_value_t root;
	jamerson_string_t jstring;
	const char *string;
	size_t i;

	for (i = 0; i < arraycount(parser_string_success_testcases); ++i) {
		t = &parser_string_success_testcases[i];
		fp = fmemopen((void *)t->var, t->lenvar, "r");
		ATF_CHECK(fp != NULL);
		root = jamerson_load_file(fp);
		ATF_CHECK(root != NULL);
		jstring = jamerson_value_is_string(root);
		ATF_CHECK(jstring != NULL);
		string = jamerson_string_string_value(jstring);
		ATF_CHECK(string != NULL);
		ATF_CHECK(!strcmp(string, t->expected));
		jamerson_value_delete(root);
	}
}

ATF_TP_ADD_TCS(tp)
{
	ATF_TP_ADD_TC(tp, test_array);
	ATF_TP_ADD_TC(tp, test_boolean);
	ATF_TP_ADD_TC(tp, test_null);
	ATF_TP_ADD_TC(tp, test_number);
	ATF_TP_ADD_TC(tp, test_string);
	ATF_TP_ADD_TC(tp, test_object);
	ATF_TP_ADD_TC(tp, test_array_complex);
	ATF_TP_ADD_TC(tp, test_object_complex);
	ATF_TP_ADD_TC(tp, test_parser_fail);
	ATF_TP_ADD_TC(tp, test_parser_success);
	ATF_TP_ADD_TC(tp, test_parser_string_fail);
	ATF_TP_ADD_TC(tp, test_parser_string_success);

	return atf_no_error();
}
