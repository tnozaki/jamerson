/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "jamerson_defs.h"

#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>

#include "unicode.h"
#include "jamerson_parser.h"

static int jamerson_parse_element(FILE *,
    jamerson_event_handler_t, void *);

const char jamerson_parser_sym_true[] = {
	UTF8_LATIN_SMALL_T,
	UTF8_LATIN_SMALL_R,
	UTF8_LATIN_SMALL_U,
	UTF8_LATIN_SMALL_E,
	UTF8_CTRL_NUL,
};

const char jamerson_parser_sym_false[] = {
	UTF8_LATIN_SMALL_F,
	UTF8_LATIN_SMALL_A,
	UTF8_LATIN_SMALL_L,
	UTF8_LATIN_SMALL_S,
	UTF8_LATIN_SMALL_E,
	UTF8_CTRL_NUL,
};

const char jamerson_parser_sym_null[] = {
	UTF8_LATIN_SMALL_N,
	UTF8_LATIN_SMALL_U,
	UTF8_LATIN_SMALL_L,
	UTF8_LATIN_SMALL_L,
	UTF8_CTRL_NUL,
};

static inline void
skip_ws(FILE *fp)
{
	int ch;

	while ((ch = getc_unlocked(fp)) != EOF) {
		if (!utf8_isspace(ch)) {
			ungetc(ch, fp);
			break;
		}
	}
}

static inline bool
jamerson_match_keyword(FILE *fp, const char *s)
{
	int ch;

	while (*s) {
		ch = getc_unlocked(fp);
		if (*s != ch)
			return false;
		++s;
	}
	return true;
}

static inline int
jamerson_parse_utf16(FILE *fp, uint16_t *result)
{
	uint16_t acc;
	size_t i;
	int ch, digit;

	acc = 0;
	for (i = 0; i < 4; ++i) {
		ch = getc_unlocked(fp);
		if (ch == EOF)
			return 1;
		digit = utf8_digittoint(ch);
		if (digit >= 16)
			return 1;
		acc *= 16;
		acc += digit;
	}
	*result = acc;
	return 0;
}

static inline int
jamerson_parse_unicode(FILE *fp, FILE *sb)
{
	char utf8[UTF8_BUFSIZ];
	uint16_t hi, lo;
	int ch;
	uint32_t utf32;
	size_t len, i;

	if (jamerson_parse_utf16(fp, &hi))
		return 1;
	if (is_hi_surrogate(hi)) {
		ch = getc_unlocked(fp);
		if (ch != UTF8_REVERSE_SOLIDUS)
			return 1;
		ch = getc_unlocked(fp);
		if (ch != UTF8_LATIN_SMALL_U)
			return 1;
		if (jamerson_parse_utf16(fp, &lo))
			return 1;
		if (!is_lo_surrogate(lo))
			return 1;
		utf32 = utf16to32(hi, lo);
	} else {
		utf32 = hi;
	}
	if (utf32to8(utf8, sizeof(utf8), utf32, &len))
		return 1;
	for (i = 0; i < len; ++i) {
		if (putc_unlocked((unsigned char)utf8[i], sb) == EOF)
			return 1;
	}
	return 0;
}

static inline int
jamerson_parse_character(FILE *fp, FILE *sb)
{
	int ch;

	ch = getc_unlocked(fp);
	switch (ch) {
	case UTF8_CTRL_NUL:
	case UTF8_CTRL_SOH:
	case UTF8_CTRL_STX:
	case UTF8_CTRL_ETX:
	case UTF8_CTRL_EOT:
	case UTF8_CTRL_ENQ:
	case UTF8_CTRL_ACK:
	case UTF8_CTRL_BEL:
	case UTF8_CTRL_VT:
	case UTF8_CTRL_SO:
	case UTF8_CTRL_SI:
	case UTF8_CTRL_DLE:
	case UTF8_CTRL_DC1:
	case UTF8_CTRL_DC2:
	case UTF8_CTRL_DC3:
	case UTF8_CTRL_DC4:
	case UTF8_CTRL_NAK:
	case UTF8_CTRL_SYN:
	case UTF8_CTRL_ETB:
	case UTF8_CTRL_CAN:
	case UTF8_CTRL_EM:
	case UTF8_CTRL_SUB:
	case UTF8_CTRL_ESC:
	case UTF8_CTRL_FS:
	case UTF8_CTRL_GS:
	case UTF8_CTRL_RS:
	case UTF8_CTRL_US:
		return 1;
	case UTF8_REVERSE_SOLIDUS:
		ch = getc_unlocked(fp);
		switch (ch) {
		case UTF8_QUOTATION_MARK:
		case UTF8_REVERSE_SOLIDUS:
		case UTF8_SOLIDUS:
			break;
		case UTF8_LATIN_SMALL_B:
			ch = UTF8_CTRL_BS;
			break;
		case UTF8_LATIN_SMALL_F:
			ch = UTF8_CTRL_FF;
			break;
		case UTF8_LATIN_SMALL_N:
			ch = UTF8_CTRL_LF;
			break;
		case UTF8_LATIN_SMALL_R:
			ch = UTF8_CTRL_CR;
			break;
		case UTF8_LATIN_SMALL_T:
			ch = UTF8_CTRL_HT;
			break;
		case UTF8_LATIN_SMALL_U:
			return jamerson_parse_unicode(fp, sb);
		default:
			return 1;
		}
		break;
	}
	if (putc_unlocked(ch, sb) == EOF)
		return 1;
	return 0;
}

static int
jamerson_parse_string(FILE *fp, char **result)
{
	FILE *sb;
	char *s;
	size_t n;
	int ch;

	sb = open_memstream(&s, &n);
	if (sb == NULL)
		return 1;
	for (;;) {
		ch = getc_unlocked(fp);
		if (ch == EOF)
			goto fatal;
		if (ch == UTF8_QUOTATION_MARK)
			break;
		ungetc(ch, fp);
		if (jamerson_parse_character(fp, sb))
			goto fatal;
	}
	if (fflush(sb))
		goto fatal;
	fclose(sb);
	*result = s;
	return 0;

fatal:
	fclose(sb);
	free(s);
	return 1;
}

static inline int
jamerson_parse_sig(FILE *fp, FILE *sb)
{
	int ch;

	ch = getc_unlocked(fp);
	if (ch == EOF)
		return 1;
	if (ch == UTF8_HYPHEN_MINUS) {
		if (putc_unlocked(ch, sb) == EOF)
			return 1;
	} else {
		ungetc(ch, fp);
	}
	return 0;
}

static inline int
jamerson_parse_digits(FILE *fp, FILE *sb)
{
	int ch;

	for (;;) {
		ch = getc_unlocked(fp);
		if (ch == EOF)
			return 1;
		if (utf8_digittoint(ch) >= 10)
			break;
		if (putc_unlocked(ch, sb) == EOF)
			return 1;
	}
	ungetc(ch, fp);
	return 0;
}

static inline int
jamerson_parse_int(FILE *fp, FILE *sb)
{
	int ch;

	ch = getc_unlocked(fp);
	if (ch == EOF)
		return 1;
	if (utf8_digittoint(ch) >= 10)
		return 1;
	if (putc_unlocked(ch, sb) == EOF)
		return 1;
	if (ch != UTF8_DIGIT_ZERO) {
		if (jamerson_parse_digits(fp, sb))
			return 1;
	}
	return 0;
}

static inline int
jamerson_parse_fra(FILE *fp, FILE *sb)
{
	int ch;

	ch = getc_unlocked(fp);
	if (ch == EOF)
		return 1;
	if (ch == UTF8_FULL_STOP) {
		if (putc_unlocked(ch, sb) == EOF)
			return 1;
		if (jamerson_parse_digits(fp, sb))
			return 1;
	} else {
		ungetc(ch, fp);
	}
	return 0;
}

static inline int
jamerson_parse_exp(FILE *fp, FILE *sb)
{
	int ch;

	ch = getc_unlocked(fp);
	if (ch == EOF)
		return 1;
	if (ch == UTF8_LATIN_CAPITAL_E || ch == UTF8_LATIN_SMALL_E) {
		if (putc_unlocked(ch, sb) == EOF)
			return 1;
		ch = getc_unlocked(fp);
		if (ch == EOF)
			return 1;
		if (ch == UTF8_PLUS_SIGN || ch == UTF8_HYPHEN_MINUS) {
			if (putc_unlocked(ch, sb) == EOF)
				return 1;
		}
		if (jamerson_parse_digits(fp, sb))
			return 1;
	} else {
		ungetc(ch, fp);
	}
	return 0;
}

static int
jamerson_parse_value_array(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	int ch;

	if ((*handler)(closure, BEGIN_ARRAY))
		return 1;
	skip_ws(fp);
	ch = getc_unlocked(fp);
	if (ch != UTF8_RIGHT_SQUARE_BRACKET) {
		ungetc(ch, fp);
		do {
			if (jamerson_parse_element(fp, handler, closure))
				return 1;
			skip_ws(fp);
			ch = getc_unlocked(fp);
		} while (ch == UTF8_COMMA);
		if (ch != UTF8_RIGHT_SQUARE_BRACKET)
			return 1;
	}
	return (*handler)(closure, END_ARRAY);
}

static inline int
jamerson_parse_value_boolean_true(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	if (jamerson_match_keyword(fp, &jamerson_parser_sym_true[1]) == false)
		return 1;
	return (*handler)(closure, VALUE_BOOLEAN, true);
}

static inline int
jamerson_parse_value_boolean_false(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	if (jamerson_match_keyword(fp, &jamerson_parser_sym_false[1]) == false)
		return 1;
	return (*handler)(closure, VALUE_BOOLEAN, false);
}

static inline int
jamerson_parse_value_null(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	if (jamerson_match_keyword(fp, &jamerson_parser_sym_null[1]) == false)
		return 1;
	return (*handler)(closure, VALUE_NULL);
}

static inline int
jamerson_parse_value_number(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	int ret;
	FILE *sb;
	char *s;
	size_t n;
	double d;

	ret = 1;
	sb = open_memstream(&s, &n);
	if (sb != NULL) {
		if (jamerson_parse_sig(fp, sb) == 0 &&
		    jamerson_parse_int(fp, sb) == 0 &&
		    jamerson_parse_fra(fp, sb) == 0 &&
		    jamerson_parse_exp(fp, sb) == 0 &&
		    fflush(sb) == 0) {
			d = utf8_strtod((const char *)s, NULL);
			if (d != HUGE_VAL && d != -HUGE_VAL)
				ret = (*handler)(closure, VALUE_NUMBER, d);
		}
		fclose(sb);
		free(s);
	}
	return ret;
}

static int
jamerson_parse_value_object(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	int ch;
	char *key;

	if ((*handler)(closure, BEGIN_OBJECT))
		return 1;
	skip_ws(fp);
	ch = getc_unlocked(fp);
	if (ch != UTF8_RIGHT_CURLY_BRACKET) {
		for (;;) {
			if (ch != UTF8_QUOTATION_MARK)
				return 1;
			if (jamerson_parse_string(fp, &key))
				return 1;
			if ((*handler)(closure, KEY_NAME, key)) {
				free(key);
				return 1;
			}
			free(key);
			skip_ws(fp);
			ch = getc_unlocked(fp);
			if (ch != UTF8_COLON)
				return 1;
			if (jamerson_parse_element(fp, handler, closure))
				return 1;
			skip_ws(fp);
			ch = getc_unlocked(fp);
			if (ch == UTF8_RIGHT_CURLY_BRACKET)
				break;
			if (ch != UTF8_COMMA)
				return 1;
			skip_ws(fp);
			ch = getc_unlocked(fp);
		}
	}
	return (*handler)(closure, END_OBJECT);
}

static inline int
jamerson_parse_value_string(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	char *s;
	int ret;

	if (jamerson_parse_string(fp, &s))
		return 1;
	ret = (*handler)(closure, VALUE_STRING, s);
	free(s);
	return ret;
}

static int
jamerson_parse_element(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	int ch;

	skip_ws(fp);
	ch = getc_unlocked(fp);
	switch (ch) {
	case UTF8_LATIN_SMALL_F:
		return jamerson_parse_value_boolean_false(fp,
		    handler, closure);
	case UTF8_LATIN_SMALL_T:
		return jamerson_parse_value_boolean_true(fp,
		    handler, closure);
	case UTF8_LATIN_SMALL_N:
		return jamerson_parse_value_null(fp, handler, closure);
	case UTF8_QUOTATION_MARK:
		return jamerson_parse_value_string(fp, handler, closure);
	case UTF8_LEFT_CURLY_BRACKET:
		return jamerson_parse_value_object(fp, handler, closure);
	case UTF8_LEFT_SQUARE_BRACKET:
		return jamerson_parse_value_array(fp, handler, closure);
	case UTF8_HYPHEN_MINUS:
	case UTF8_DIGIT_ZERO:
	case UTF8_DIGIT_ONE:
	case UTF8_DIGIT_TWO:
	case UTF8_DIGIT_THREE:
	case UTF8_DIGIT_FOUR:
	case UTF8_DIGIT_FIVE:
	case UTF8_DIGIT_SIX:
	case UTF8_DIGIT_SEVEN:
	case UTF8_DIGIT_EIGHT:
	case UTF8_DIGIT_NINE:
		ungetc(ch, fp);
		return jamerson_parse_value_number(fp, handler, closure);
	}
	return 1;
}

int
jamerson_parse_from_file(FILE *fp, jamerson_event_handler_t handler,
    void *closure)
{
	int ch, ret;

	ret = jamerson_parse_element(fp, handler, closure);
	if (ret)
		return ret;
	skip_ws(fp);
	ch = getc_unlocked(fp);
	if (ch != UTF8_CTRL_NUL && ch != EOF && !feof(fp))
		return 1;
	return 0;
}
