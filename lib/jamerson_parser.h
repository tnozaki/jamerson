/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef JAMERSON_PARSER_H_
#define JAMERSON_PARSER_H_

#include <stdio.h>

typedef enum {
	BEGIN_ARRAY,
	END_ARRAY,
	VALUE_BOOLEAN,
	VALUE_NULL,
	VALUE_NUMBER,
	BEGIN_OBJECT,
	KEY_NAME,
	END_OBJECT,
	VALUE_STRING
} jamerson_event_t;

typedef int (*jamerson_event_handler_t)(void *, jamerson_event_t, ...);

#if defined(__cplusplus)
extern "C" {
#endif

int jamerson_parse_from_file(FILE *fp,
    jamerson_event_handler_t handler, void *closure);

#if defined(__cplusplus)
}
#endif

#endif /*!JAMERSON_PARSER_H_*/
