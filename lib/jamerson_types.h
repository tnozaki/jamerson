/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef JAMERSON_TYPES_H_
#define JAMERSON_TYPES_H_

#include <stddef.h>
#include <stdbool.h>

typedef struct jamerson_value   *jamerson_value_t;
typedef struct jamerson_array   *jamerson_array_t;
typedef struct jamerson_boolean *jamerson_boolean_t;
typedef struct jamerson_null    *jamerson_null_t;
typedef struct jamerson_number  *jamerson_number_t;
typedef struct jamerson_object  *jamerson_object_t;
typedef struct jamerson_string  *jamerson_string_t;

#if defined(__cplusplus)
extern "C" {
#endif

/* jamerson_value_t */
jamerson_value_t   jamerson_value_new_array(jamerson_array_t);
jamerson_value_t   jamerson_value_new_boolean(jamerson_boolean_t);
jamerson_value_t   jamerson_value_new_null(jamerson_null_t);
jamerson_value_t   jamerson_value_new_number(jamerson_number_t);
jamerson_value_t   jamerson_value_new_object(jamerson_object_t);
jamerson_value_t   jamerson_value_new_string(jamerson_string_t);
void               jamerson_value_delete(jamerson_value_t);
jamerson_value_t   jamerson_value_get_parent(jamerson_value_t);
jamerson_value_t   jamerson_value_set_parent(jamerson_value_t,
                                             jamerson_value_t);
jamerson_array_t   jamerson_value_is_array(jamerson_value_t);
jamerson_boolean_t jamerson_value_is_boolean(jamerson_value_t);
jamerson_null_t    jamerson_value_is_null(jamerson_value_t);
jamerson_number_t  jamerson_value_is_number(jamerson_value_t);
jamerson_object_t  jamerson_value_is_object(jamerson_value_t);
jamerson_string_t  jamerson_value_is_string(jamerson_value_t);

/* jamerson_array_t */
jamerson_array_t jamerson_array_new(void);
void             jamerson_array_delete(jamerson_array_t);
int              jamerson_array_push_back(jamerson_array_t, jamerson_value_t);
size_t           jamerson_array_size(jamerson_array_t);
jamerson_value_t jamerson_array_get(jamerson_array_t, size_t);

/* jamerson_boolean_t */
jamerson_boolean_t jamerson_boolean_new(bool);
void               jamerson_boolean_delete(jamerson_boolean_t);
bool               jamerson_boolean_boolean_value(jamerson_boolean_t);

/* jamerson_null_t */
jamerson_null_t jamerson_null_new(void);
void            jamerson_null_delete(jamerson_null_t);

/* jamerson_number_t */
jamerson_number_t jamerson_number_new(double);
void              jamerson_number_delete(jamerson_number_t);
double            jamerson_number_double_value(jamerson_number_t);

/* jamerson_object_t */
jamerson_object_t    jamerson_object_new(void);
void                 jamerson_object_delete(jamerson_object_t);
int                  jamerson_object_put(jamerson_object_t, const char *,
                                         jamerson_value_t, jamerson_value_t *);
jamerson_value_t     jamerson_object_get(jamerson_object_t, const char *);
const char * const * jamerson_object_key_set(jamerson_object_t, size_t *);

/* jamerson_string_t */
jamerson_string_t jamerson_string_new(const char *);
void              jamerson_string_delete(jamerson_string_t);
const char *      jamerson_string_string_value(jamerson_string_t);

#if defined(__cplusplus)
}
#endif

#endif /*!JAMERSON_TYPES_H_*/
