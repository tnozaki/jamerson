/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <stdlib.h>

#include "jamerson_local.h"

#define JAMERSON_ARRAY_DEFAULT_CAPACITY 32
#define JAMERSON_ARRAY_MAX_CAPACITY \
	(SIZE_MAX / sizeof(jamerson_value_t))

jamerson_array_t
jamerson_array_new(void)
{
	jamerson_array_t p;

	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->capacity = JAMERSON_ARRAY_DEFAULT_CAPACITY;
		p->size = 0;
		p->head = malloc(sizeof(*p->head) * p->capacity);
		if (p->head != NULL)
			return p;
		free(p);
	}
	return NULL;
}

void
jamerson_array_delete(jamerson_array_t p)
{
	size_t len;
	jamerson_value_t value;

	len = p->size;
	while (p->size > 0) {
		value = p->head[--p->size];
		jamerson_value_delete(value);
	}
	free(p->head);
	free(p);
}

int
jamerson_array_push_back(jamerson_array_t p, jamerson_value_t value)
{
	size_t capacity;
	jamerson_value_t *head;

	if (p->capacity == p->size) {
		if (JAMERSON_ARRAY_MAX_CAPACITY / 2 < p->capacity)
			return 1;
		capacity = p->capacity * 2;
		head = realloc(p->head,
		    sizeof(jamerson_value_t) * capacity);
		if (head == NULL)
			return 1;
		p->head = head;
		p->capacity = capacity;
	}
	p->head[p->size++] = value;
	return 0;
}

size_t
jamerson_array_size(jamerson_array_t p)
{
	return p->size;
}

jamerson_value_t
jamerson_array_get(jamerson_array_t p, size_t i)
{
	if (p->size > i)
		return p->head[i];
	return NULL;
}
