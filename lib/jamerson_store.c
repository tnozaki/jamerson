/*-
 * Copyright (c) 2021 Takehiko NOZAKI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "unicode.h"
#include "jamerson_local.h"
#include "jamerson_store.h"

#if defined(__NetBSD__)
extern char *__dtoa(double, int, int, int *, int *, char **);
extern void __freedtoa(char *);
#define dtoa            __dtoa
#define freedtoa        __freedtoa
#else
#include "gdtoa.h"
#endif

static int jamerson_encode_value(jamerson_value_t, FILE *, int, int);

static const char sym_name_separator[] = {
	UTF8_COLON,
	UTF8_CTRL_NUL
};

static const char sym_name_separator_pretty_print[] = {
	UTF8_SPACE,
	UTF8_COLON,
	UTF8_SPACE,
	UTF8_CTRL_NUL
};

#define PADSIZ  20
static const char zeros[PADSIZ] = {
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ZERO, UTF8_DIGIT_ZERO,
};

static inline int
jamerson_indent(FILE *fp, int flags, int level)
{
	if (flags & JAMERSON_PRETTY_PRINT) {
		if (fputc(UTF8_CTRL_LF, fp) == EOF)
			return 1;
		while (level-- > 0) {
			if (fputc(UTF8_CTRL_HT, fp) == EOF)
				return 1;
		}
	}
	return 0;
}

static int
jamerson_encode_string_value(const char *s, FILE *fp, int flags)
{
	const char *t;
	int ch;
	size_t len, n;
	uint32_t utf32;
	char buf[UNICODE_ESCAPE_BUFSIZ + 1];

	if (fputc(UTF8_QUOTATION_MARK, fp) == EOF)
		return 1;
	t = s;
	while ((ch = *t++) != UTF8_CTRL_NUL) {
		switch (ch) {
		case UTF8_CTRL_SOH:
		case UTF8_CTRL_STX:
		case UTF8_CTRL_ETX:
		case UTF8_CTRL_EOT:
		case UTF8_CTRL_ENQ:
		case UTF8_CTRL_ACK:
		case UTF8_CTRL_BEL:
		case UTF8_CTRL_VT:
		case UTF8_CTRL_SO:
		case UTF8_CTRL_SI:
		case UTF8_CTRL_DLE:
		case UTF8_CTRL_DC1:
		case UTF8_CTRL_DC2:
		case UTF8_CTRL_DC3:
		case UTF8_CTRL_DC4:
		case UTF8_CTRL_NAK:
		case UTF8_CTRL_SYN:
		case UTF8_CTRL_ETB:
		case UTF8_CTRL_CAN:
		case UTF8_CTRL_EM:
		case UTF8_CTRL_SUB:
		case UTF8_CTRL_ESC:
		case UTF8_CTRL_FS:
		case UTF8_CTRL_GS:
		case UTF8_CTRL_RS:
		case UTF8_CTRL_US:
			return 1;
		case UTF8_QUOTATION_MARK:
		case UTF8_REVERSE_SOLIDUS:
		case UTF8_SOLIDUS:
			break;
		case UTF8_CTRL_BS:
			ch = UTF8_LATIN_SMALL_B;
			break;
		case UTF8_CTRL_FF:
			ch = UTF8_LATIN_SMALL_F;
			break;
		case UTF8_CTRL_LF:
			ch = UTF8_LATIN_SMALL_N;
			break;
		case UTF8_CTRL_CR:
			ch = UTF8_LATIN_SMALL_R;
			break;
		case UTF8_CTRL_HT:
			ch = UTF8_LATIN_SMALL_T;
			break;
		default:
			if ((flags & JAMERSON_UNICODE_ESCAPE)) {
				--t;
				if (t[1] == UTF8_CTRL_NUL)
					len = 1;
				else if (t[2] == UTF8_CTRL_NUL)
					len = 2;
				else if (t[3] == UTF8_CTRL_NUL)
					len = 3;
				else
					len = 4;
				if (utf8to32(t, len, &utf32, &n))
					return 1;
				t += n;
				if (escape_utf32(buf, sizeof(buf), utf32, &n))
					return 1;
				buf[n] = '\0';
				if (fputs(buf, fp) == EOF)
					return 1;
			} else {
				if (fputc(ch, fp) == EOF)
					return 1;
			}
			continue;
		}
		if (fputc(UTF8_QUOTATION_MARK, fp) == EOF)
			return 1;
		if (fputc(ch, fp) == EOF)
			return 1;
	}
	if (fputc(UTF8_QUOTATION_MARK, fp) == EOF)
		return 1;
	return 0;
}

static int
jamerson_encode_array(jamerson_array_t jarray,
    FILE *fp, int flags, int level)
{
	size_t siz, i;

	if (fputc(UTF8_LEFT_SQUARE_BRACKET, fp) == EOF)
		return 1;
	siz = jarray->size;
	if (siz > 0) {
		for (i = 0; i < siz; ++i) {
			if (i > 0) {
				if (fputc(UTF8_COMMA, fp) == EOF)
					return 1;
			}
			if (jamerson_indent(fp, flags, level + 1))
				return 1;
			if (jamerson_encode_value(jarray->head[i],
			    fp, flags, level + 1))
				return 1;
		}
		if (jamerson_indent(fp, flags, level))
			return 1;
	}
	if (fputc(UTF8_RIGHT_SQUARE_BRACKET, fp) == EOF)
		return 1;
	return 0;
}

static int
jamerson_encode_boolean(jamerson_boolean_t jboolean, FILE *fp, int flags)
{
	if (fputs(jboolean->string, fp) == EOF)
		return 1;
	return 0;
}

static int
jamerson_encode_null(jamerson_null_t jnull, FILE *fp, int flags)
{
	if (fputs(jnull->string, fp) == EOF)
		return 1;
	return 0;
}

static inline int
jamerson_pad(const char *filler, int len, FILE *fp)
{
	while (len > PADSIZ) {
		if (fwrite(filler, 1, PADSIZ, fp) != PADSIZ)
			return 1;
		len -= PADSIZ;
	}
	if (fwrite(filler, 1, len, fp) != len)
		return 1;
	return 0;
}

static inline int
jamerson_efmt(char *head, int len, int exp, int neg, FILE *fp)
{
#define MAXEXPSIZ	4
	char expstr[MAXEXPSIZ + 2];

	if (neg && putc(UTF8_HYPHEN_MINUS, fp) == EOF)
		return 1;
	if (putc(*head, fp) == EOF)
		return 1;
	if (--len > 0) {
		if (putc(UTF8_FULL_STOP, fp) == EOF)
			return 1;
		if (fwrite(&head[1], 1, len, fp) != len)
			return 1;
	}
	expstr[0] = UTF8_LATIN_SMALL_E;
	if (exp >= 0) {
		expstr[1] = UTF8_PLUS_SIGN;
	} else {
		expstr[1] = UTF8_HYPHEN_MINUS;
		exp = -exp;
	}
	/* assume DBL_MIN_EXP(-1021) DBL_MAX_EXP(1024) */
	if (exp < 10) {
		expstr[2] = utf8_inttodigit(exp);
		len = 3;
	} else if (exp < 100) {
		expstr[2] = utf8_inttodigit(exp / 10);
		expstr[3] = utf8_inttodigit(exp % 10);
		len = 4;
	} else if (exp < 1000) {
		expstr[2] = utf8_inttodigit(exp / 100);
		expstr[3] = utf8_inttodigit((exp % 100) / 10);
		expstr[4] = utf8_inttodigit(exp % 10);
		len = 5;
	} else {
		expstr[2] = utf8_inttodigit(exp / 1000);
		expstr[3] = utf8_inttodigit((exp % 1000) / 100);
		expstr[4] = utf8_inttodigit((exp % 100) / 10);
		expstr[5] = utf8_inttodigit(exp % 10);
		len = 6;
	}
	if (fwrite(expstr, 1, len, fp) != len)
		return 1;
	return 0;
}

static inline int
jamerson_ffmt(char *head, int len, int exp, int neg, FILE *fp)
{
	if (neg && putc(UTF8_HYPHEN_MINUS, fp) == EOF)
		return 1;
	if (len <= exp) {
		if (fwrite(head, 1, len, fp) != len)
			return 1;
		if (jamerson_pad(zeros, exp - len, fp))
			return 1;
	} else if (exp > 0) {
		if (fwrite(head, 1, exp, fp) != exp)
			return 1;
		if (putc(UTF8_FULL_STOP, fp) == EOF)
			return 1;
		len -= exp;
		if (fwrite(&head[exp], 1, len, fp) != len)
			return 1;
	} else {
		if (putc(UTF8_DIGIT_ZERO, fp) == EOF)
			return 1;
		if (putc(UTF8_FULL_STOP, fp) == EOF)
			return 1;
		exp = -exp;
		if (exp > 0 && jamerson_pad(zeros, exp, fp))
			return 1;
		if (fwrite(head, 1, len, fp) != len)
			return 1;
	}
	return 0;
}

static inline int
jamerson_gfmt(char *head, int len, int exp, int neg, FILE *fp)
{
	return (exp > -4 && exp <= len + 5)
	    ? jamerson_ffmt(head, len, exp, neg, fp)
	    : jamerson_efmt(head, len, exp - 1, neg, fp);
}

static int
jamerson_encode_double_value(double dvalue, FILE *fp)
{
	int exp, neg, len, ret;
	char *head, *tail, *tmp;

	head = dtoa(dvalue, 0, 0, &exp, &neg, &tail);
	if (head == NULL)
		return 1;
	if (exp == 9999)
		goto fatal;
	len = tail - head;
	tmp = strndup(head, len);
	if (tmp == NULL)
		goto fatal;
	freedtoa(head);
	head = tmp;
	tail = head + len;
	while (tmp < tail) {
		switch (*tmp) {
		case '0':
			*tmp = UTF8_DIGIT_ZERO;
			break;
		case '1':
			*tmp = UTF8_DIGIT_ONE;
			break;
		case '2':
			*tmp = UTF8_DIGIT_TWO;
			break;
		case '3':
			*tmp = UTF8_DIGIT_THREE;
			break;
		case '4':
			*tmp = UTF8_DIGIT_FOUR;
			break;
		case '5':
			*tmp = UTF8_DIGIT_FIVE;
			break;
		case '6':
			*tmp = UTF8_DIGIT_SIX;
			break;
		case '7':
			*tmp = UTF8_DIGIT_SEVEN;
			break;
		case '8':
			*tmp = UTF8_DIGIT_EIGHT;
			break;
		case '9':
			*tmp = UTF8_DIGIT_NINE;
			break;
		}
		++tmp;
	}
	ret = jamerson_gfmt(head, len, exp, neg, fp);
	free(head);
	return ret;
fatal:
	freedtoa(head);
	return 1;
}

static int
jamerson_encode_number(jamerson_number_t jnumber, FILE *fp, int flags)
{
	return jamerson_encode_double_value(jnumber->value, fp);
}

static int
jamerson_key_compare(const void *arg1, const void *arg2)
{
	return strcmp(*((const char **)arg1), *((const char **)arg2));
}

static int
jamerson_encode_object(jamerson_object_t jobject,
    FILE *fp, int flags, int level)
{
	int retval;
	const char **keys, *key, *name_separator;
	const char * const *tmp;
	size_t siz, i;
	jamerson_value_t value;

	retval = 1;
	keys = NULL;
	if (fputc(UTF8_LEFT_CURLY_BRACKET, fp) == EOF)
		goto fatal;
	tmp = jamerson_object_key_set(jobject, &siz);
	assert(siz <= (SIZE_MAX / sizeof(*tmp)));
	if (siz > 0) {
		keys = malloc(siz * sizeof(*keys));
		if (keys == NULL)
			return 1;
		memcpy(keys, tmp, siz * sizeof(*keys));
		if (flags & JAMERSON_CANONICAL)
			qsort((void *)keys, siz, sizeof(*keys),
			    jamerson_key_compare);
		name_separator = (flags & JAMERSON_PRETTY_PRINT)
		    ? sym_name_separator_pretty_print
		    : sym_name_separator;
		for (i = 0; i < siz; ++i) {
			key = keys[i];
			if (i > 0) {
				if (fputc(UTF8_COMMA, fp) == EOF)
					goto fatal;
			}
			if (jamerson_indent(fp, flags, level + 1))
				return 1;
			value = jamerson_object_get(jobject, key);
			if (jamerson_encode_string_value(key, fp, flags))
				goto fatal;
			if (fputs(name_separator, fp) == EOF)
				goto fatal;
			if (jamerson_encode_value(value, fp, flags, level + 1))
				goto fatal;
		}
		if (jamerson_indent(fp, flags, level))
			return 1;
	}
	if (fputc(UTF8_RIGHT_CURLY_BRACKET, fp) == EOF)
		goto fatal;
	retval = 0;
fatal:
	free(keys);
	return retval;
}

static int
jamerson_encode_string(jamerson_string_t jstring, FILE *fp, int flags)
{
	return jamerson_encode_string_value(jstring->value, fp, flags);
}

static int
jamerson_encode_value(jamerson_value_t value,
    FILE *fp, int flags, int level)
{
	if (value != NULL) {
		switch (value->type) {
		case JAMERSON_VALUE_TYPE_ARRAY:
			return jamerson_encode_array(value->u.array_value,
			    fp, flags, level);
		case JAMERSON_VALUE_TYPE_BOOLEAN:
			return jamerson_encode_boolean(value->u.boolean_value,
			    fp, flags);
		case JAMERSON_VALUE_TYPE_NULL:
			return jamerson_encode_null(value->u.null_value,
			    fp, flags);
		case JAMERSON_VALUE_TYPE_NUMBER:
			return jamerson_encode_number(value->u.number_value,
			    fp, flags);
		case JAMERSON_VALUE_TYPE_OBJECT:
			return jamerson_encode_object(value->u.object_value,
			    fp, flags, level);
		case JAMERSON_VALUE_TYPE_STRING:
			return jamerson_encode_string(value->u.string_value,
			    fp, flags);
		}
	}
	return 1;
}

int
jamerson_store_file(jamerson_value_t value, FILE *fp, int flags)
{
	if (jamerson_encode_value(value, fp, flags, 0))
		return 1;
	if (flags & JAMERSON_PRETTY_PRINT) {
		if (fputc(UTF8_CTRL_LF, fp) == EOF)
			return 1;
	}
	return 0;
}
