/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef JAMERSON_LOCAL_H_
#define JAMERSON_LOCAL_H_

#include "jamerson_defs.h"

#include <sys/tree.h>
#include <stdint.h>

#include <algorithm/vector.h>

#include "jamerson_types.h"

extern const char jamerson_parser_sym_true[];
extern const char jamerson_parser_sym_false[];
extern const char jamerson_parser_sym_null[];

typedef enum {
	JAMERSON_VALUE_TYPE_ARRAY,
	JAMERSON_VALUE_TYPE_BOOLEAN,
	JAMERSON_VALUE_TYPE_NULL,
	JAMERSON_VALUE_TYPE_NUMBER,
	JAMERSON_VALUE_TYPE_OBJECT,
	JAMERSON_VALUE_TYPE_STRING
} jamerson_value_type_t;

struct jamerson_value {
	jamerson_value_type_t type;
	union {
		jamerson_array_t	array_value;
		jamerson_boolean_t	boolean_value;
		jamerson_null_t		null_value;
		jamerson_number_t	number_value;
		jamerson_object_t	object_value;
		jamerson_string_t	string_value;
	} u;
	jamerson_value_t parent;
};

struct jamerson_array {
	jamerson_value_t *head;
	size_t size;
	size_t capacity;
};

struct jamerson_boolean {
	bool value;
	const char *string;
};

struct jamerson_null {
	const char *string;
};

struct jamerson_number {
	double value;
};

struct jamerson_rb_node {
	RB_ENTRY(jamerson_rb_node) rbn;
	uint32_t hash;
	jamerson_value_t value;
	char key[];
};
RB_HEAD(jamerson_rb_tree, jamerson_rb_node);

VECTOR(jamerson_keys, const char *);

struct jamerson_object {
	struct jamerson_rb_tree rbt;
	struct jamerson_keys *keys;
};

struct jamerson_string {
	char *value;
};

#endif /*!JAMERSON_LOCAL_H_*/
