/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "jamerson_parser.h"
#include "jamerson_local.h"
#include "jamerson_load.h"

struct jamerson_document {
	jamerson_value_t root, locator;
	char *key;
};

static int
jamerson_event_handler_store(struct jamerson_document *document,
    jamerson_value_t value)
{
	jamerson_value_t old;
	int ret;

	if (document->locator == NULL) {
		document->root = document->locator = value;
		return 0;
	}
	switch (document->locator->type) {
	case JAMERSON_VALUE_TYPE_ARRAY:
		return jamerson_array_push_back(
		    document->locator->u.array_value, value);
	case JAMERSON_VALUE_TYPE_OBJECT:
		ret = jamerson_object_put(document->locator->u.object_value,
		    document->key, value, &old);
		free(document->key);
		document->key = NULL;
		if (ret)
			return ret;
		if (old != NULL)
			jamerson_value_delete(old);
		return 0;
	default:
		return 1;
	}
}

static inline int
jamerson_event_handler_begin_array(struct jamerson_document *document,
    va_list ap)
{
	jamerson_array_t array_value;
	jamerson_value_t value;
	int ret;

	array_value = jamerson_array_new();
	if (array_value == NULL)
		return 1;
	value = jamerson_value_new_array(array_value);
	if (value == NULL) {
		jamerson_array_delete(array_value);
		return 1;
	}
	jamerson_value_set_parent(value, document->locator);
	ret = jamerson_event_handler_store(document, value);
	if (ret)
		return ret;
	document->locator = value;
	return 0;
}

static inline int
jamerson_event_handler_end_array(struct jamerson_document *document,
    va_list ap)
{
	if (document->locator == NULL)
		return 1;
	document->locator = jamerson_value_get_parent(document->locator);
	return 0;
}

static inline int
jamerson_event_handler_value_boolean(struct jamerson_document *document,
    va_list ap)
{
	jamerson_boolean_t boolean_value;
	jamerson_value_t value;
	bool b;

	b = (bool)va_arg(ap, int);
	boolean_value = jamerson_boolean_new(b);
	if (boolean_value == NULL)
		return 1;
	value = jamerson_value_new_boolean(boolean_value);
	if (value == NULL) {
		jamerson_boolean_delete(boolean_value);
		return 1;
	}
	jamerson_value_set_parent(value, document->locator);
	return jamerson_event_handler_store(document, value);
}

static inline int
jamerson_event_handler_value_null(struct jamerson_document *document,
    va_list ap)
{
	jamerson_null_t null_value;
	jamerson_value_t value;

	null_value = jamerson_null_new();
	if (null_value == NULL)
		return 1;
	value = jamerson_value_new_null(null_value);
	if (value == NULL) {
		jamerson_null_delete(null_value);
		return 1;
	}
	jamerson_value_set_parent(value, document->locator);
	return jamerson_event_handler_store(document, value);
}

static inline int
jamerson_event_handler_value_number(struct jamerson_document *document,
    va_list ap)
{
	jamerson_number_t number_value;
	jamerson_value_t value;
	double d;

	d = (double)va_arg(ap, double);
	number_value = jamerson_number_new(d);
	if (number_value == NULL)
		return 1;
	value = jamerson_value_new_number(number_value);
	if (value == NULL) {
		jamerson_number_delete(number_value);
		return 1;
	}
	jamerson_value_set_parent(value, document->locator);
	return jamerson_event_handler_store(document, value);
}

static inline int
jamerson_event_handler_begin_object(struct jamerson_document *document,
    va_list ap)
{
	jamerson_object_t object_value;
	jamerson_value_t value;
	int ret;

	object_value = jamerson_object_new();
	if (object_value == NULL)
		return 1;
	value = jamerson_value_new_object(object_value);
	if (value == NULL) {
		jamerson_object_delete(object_value);
		return 1;
	}
	jamerson_value_set_parent(value, document->locator);
	ret = jamerson_event_handler_store(document, value);
	if (ret)
		return ret;
	document->locator = value;
	return 0;
}

static inline int
jamerson_event_handler_key_name(struct jamerson_document *document,
    va_list ap)
{
	char *key;

	key = (char *)va_arg(ap, char *);
	document->key = strdup(key);
	if (document->key == NULL)
		return errno;
	return 0;
}

static inline int
jamerson_event_handler_end_object(struct jamerson_document *document,
    va_list ap)
{
	if (document->locator == NULL)
		return 1;
	document->locator = jamerson_value_get_parent(document->locator);
	return 0;
}

static inline int
jamerson_event_handler_value_string(struct jamerson_document *document,
    va_list ap)
{
	jamerson_string_t string_value;
	jamerson_value_t value;
	const char *s;

	s = (const char *)va_arg(ap, const char *);
	string_value = jamerson_string_new(s);
	if (string_value == NULL)
		return 1;
	value = jamerson_value_new_string(string_value);
	if (value == NULL) {
		jamerson_string_delete(string_value);
		return 1;
	}
	jamerson_value_set_parent(value, document->locator);
	return jamerson_event_handler_store(document, value);
}

static int
jamerson_event_handler(void *ctx, jamerson_event_t event, ...)
{
	va_list ap;
	int ret;
	struct jamerson_document *document;

	document = (struct jamerson_document *)ctx;
	va_start(ap, event);
	switch (event) {
	/* array */
	case BEGIN_ARRAY:
		ret = jamerson_event_handler_begin_array(document, ap);
		break;
	case END_ARRAY:
		ret = jamerson_event_handler_end_array(document, ap);
		break;
	/* boolean */
	case VALUE_BOOLEAN:
		ret = jamerson_event_handler_value_boolean(document, ap);
		break;
	/* null */
	case VALUE_NULL:
		ret = jamerson_event_handler_value_null(document, ap);
		break;
	/* number */
	case VALUE_NUMBER:
		ret = jamerson_event_handler_value_number(document, ap);
		break;
	/* object */
	case BEGIN_OBJECT:
		ret = jamerson_event_handler_begin_object(document, ap);
		break;
	case KEY_NAME:
		ret = jamerson_event_handler_key_name(document, ap);
		break;
	case END_OBJECT:
		ret = jamerson_event_handler_end_object(document, ap);
		break;
	/* string */
	case VALUE_STRING:
		ret = jamerson_event_handler_value_string(document, ap);
		break;
	default:
		ret = EINVAL;
	}
	va_end(ap);
	return ret;
}

static inline int
jamerson_syntax_error(struct jamerson_document *document)
{
	if (document->root == NULL)
		return 1;
	if (document->locator != NULL) {
		if (document->root != document->locator)
			return 1;
		if (document->root->type == JAMERSON_VALUE_TYPE_ARRAY ||
		    document->root->type == JAMERSON_VALUE_TYPE_OBJECT)
			return 1;
	}
	return 0;
}

jamerson_value_t
jamerson_load_file(FILE *fp)
{
	struct jamerson_document document;

	document.root = NULL;
	document.locator = NULL;
	document.key = NULL;
	if (jamerson_parse_from_file(fp, &jamerson_event_handler,
	    (void *)&document) || jamerson_syntax_error(&document)) {
		if (document.root != NULL)
			jamerson_value_delete(document.root);
		return NULL;
	}
	return document.root;
}
