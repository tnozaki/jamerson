/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include <stdlib.h>

#include "jamerson_local.h"

jamerson_value_t
jamerson_value_new_array(jamerson_array_t value)
{
	jamerson_value_t p;

	if (value == NULL)
		return NULL;
	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->u.array_value = value;
		p->type = JAMERSON_VALUE_TYPE_ARRAY;
		p->parent = NULL;
	}
	return p;
}

jamerson_value_t
jamerson_value_new_boolean(jamerson_boolean_t value)
{
	jamerson_value_t p;

	if (value == NULL)
		return NULL;
	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->u.boolean_value = value;
		p->type = JAMERSON_VALUE_TYPE_BOOLEAN;
		p->parent = NULL;
	}
	return p;
}

jamerson_value_t
jamerson_value_new_null(jamerson_null_t value)
{
	jamerson_value_t p;

	if (value == NULL)
		return NULL;
	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->u.null_value = value;
		p->type = JAMERSON_VALUE_TYPE_NULL;
		p->parent = NULL;
	}
	return p;
}

jamerson_value_t
jamerson_value_new_number(jamerson_number_t value)
{
	jamerson_value_t p;

	if (value == NULL)
		return NULL;
	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->u.number_value = value;
		p->type = JAMERSON_VALUE_TYPE_NUMBER;
		p->parent = NULL;
	}
	return p;
}

jamerson_value_t
jamerson_value_new_object(jamerson_object_t value)
{
	jamerson_value_t p;

	if (value == NULL)
		return NULL;
	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->u.object_value = value;
		p->type = JAMERSON_VALUE_TYPE_OBJECT;
		p->parent = NULL;
	}
	return p;
}

jamerson_value_t
jamerson_value_new_string(jamerson_string_t value)
{
	jamerson_value_t p;

	if (value == NULL)
		return NULL;
	p = malloc(sizeof(*p));
	if (p != NULL) {
		p->u.string_value = value;
		p->type = JAMERSON_VALUE_TYPE_STRING;
		p->parent = NULL;
	}
	return p;
}

void
jamerson_value_delete(jamerson_value_t p)
{
	switch (p->type) {
	case JAMERSON_VALUE_TYPE_ARRAY:
		jamerson_array_delete(p->u.array_value);
		break;
	case JAMERSON_VALUE_TYPE_BOOLEAN:
		jamerson_boolean_delete(p->u.boolean_value);
		break;
	case JAMERSON_VALUE_TYPE_NULL:
		jamerson_null_delete(p->u.null_value);
		break;
	case JAMERSON_VALUE_TYPE_NUMBER:
		jamerson_number_delete(p->u.number_value);
		break;
	case JAMERSON_VALUE_TYPE_OBJECT:
		jamerson_object_delete(p->u.object_value);
		break;
	case JAMERSON_VALUE_TYPE_STRING:
		jamerson_string_delete(p->u.string_value);
		break;
	}
	free(p);
}

jamerson_array_t
jamerson_value_is_array(jamerson_value_t p)
{
	return (p != NULL && p->type == JAMERSON_VALUE_TYPE_ARRAY)
	    ? p->u.array_value : NULL;
}

jamerson_boolean_t
jamerson_value_is_boolean(jamerson_value_t p)
{
	return (p != NULL && p->type == JAMERSON_VALUE_TYPE_BOOLEAN)
	    ? p->u.boolean_value : NULL;
}

jamerson_null_t
jamerson_value_is_null(jamerson_value_t p)
{
	return (p != NULL && p->type == JAMERSON_VALUE_TYPE_NULL)
	    ? p->u.null_value : NULL;
}

jamerson_number_t
jamerson_value_is_number(jamerson_value_t p)
{
	return (p != NULL && p->type == JAMERSON_VALUE_TYPE_NUMBER)
	    ? p->u.number_value : NULL;
}

jamerson_object_t
jamerson_value_is_object(jamerson_value_t p)
{
	return (p != NULL && p->type == JAMERSON_VALUE_TYPE_OBJECT)
	    ? p->u.object_value : NULL;
}

jamerson_string_t
jamerson_value_is_string(jamerson_value_t p)
{
	return (p != NULL && p->type == JAMERSON_VALUE_TYPE_STRING)
	    ? p->u.string_value : NULL;
}

jamerson_value_t
jamerson_value_get_parent(jamerson_value_t p)
{
	return p->parent;
}

jamerson_value_t
jamerson_value_set_parent(jamerson_value_t p, jamerson_value_t parent)
{
	jamerson_value_t ret;

	ret = p->parent;
	p->parent = parent;
	return ret;
}
