/*-
 * Copyright (c)2015, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "jamerson_defs.h"
#include "unicode.h"

const unsigned char utf8tab[0x100] = {
	/* 0x0 */
	0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	0x7f, 0xff, 0xff, 0xff, 0xff, 0xff, 0x7f, 0x7f,
	/* 0x1 */
	0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	/* 0x2 */
	0xff, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	/* 0x3 */
	   0,    1,    2,    3,    4,    5,    6,    7,
	   8,    9, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	/* 0x4 */
	0x7f,   10,   11,   12,   13,   14,   15,   16,
	  17,   18,   19,   20,   21,   22,   23,   24,
	/* 0x5 */
	  25,   26,   27,   28,   29,   30,   31,   32,
	  33,   34,   35, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
	/* 0x6 */
	0x7f,   10,   11,   12,   13,   14,   15,   16,
	   17,  18,   19,   20,   21,   22,   23,   24,
	/* 0x7 */
	   25,  26,   27,   28,   29,   30,   31,   32,
	   33,  34,   35, 0x7f, 0x7f, 0x7f, 0x7f, 0x7f,
};

static const char utf8_xdigit[] = {
	UTF8_DIGIT_ZERO,
	UTF8_DIGIT_ONE,
	UTF8_DIGIT_TWO,
	UTF8_DIGIT_THREE,
	UTF8_DIGIT_FOUR,
	UTF8_DIGIT_FIVE,
	UTF8_DIGIT_SIX,
	UTF8_DIGIT_SEVEN,
	UTF8_DIGIT_EIGHT,
	UTF8_DIGIT_NINE,
	UTF8_LATIN_SMALL_A,
	UTF8_LATIN_SMALL_B,
	UTF8_LATIN_SMALL_C,
	UTF8_LATIN_SMALL_D,
	UTF8_LATIN_SMALL_E,
	UTF8_LATIN_SMALL_F,
};

static const unsigned char utf8len[0x100] = {
/*      0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f */
/* 0 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 1 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 2 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 3 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 4 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 5 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 6 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 7 */ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
/* 8 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* 9 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* a */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* b */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
/* c */ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
/* d */ 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
/* e */ 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
/* f */ 4, 4, 4, 4, 4, 4, 4, 4, 0, 0, 0, 0, 0, 0, 0, 0,
};

static const char utf8mask[] = {
	0x00, /* dummy */
	0x7f, 0x1f, 0x0f, 0x07,
};

static const unsigned char utf8bit[] = {
	0x00, /* dummy */
	0x00, 0xc0, 0xe0, 0xf0,
};

static inline int utf32len(uint32_t utf32)
{
	if (utf32 >= 0 && utf32 <= 0x7f) {
		return 1;
	} else if (utf32 >= 0x80 && utf32 <= 0x7ff) {
		return 2;
	} else if (utf32 >= 0x800 && utf32 <= 0xd7ff) {
		return 3;
	} else if (utf32 >= 0xe000 && utf32 <= 0xffff) {
		return 3;
	} else if (utf32 >= 0x10000 && utf32 <= 0x10ffff) {
		return 4;
	}
	return 0;
}

int
utf32to8(char *s, size_t n, uint32_t utf32, size_t *nresult)
{
	size_t len;
	char *t;

	len = utf32len(utf32);
	if (len == 0)
		return 1;
	if (n < len)
		return 1;
	for (t = &s[len - 1]; t > s; --t) {
		*t = (utf32 & 0x3f) | 0x80;
		utf32 >>= 6;
	}
	*t++ = utf32 |= utf8bit[len];
	*nresult = len;
	return 0;
}

int
utf8to32(const char *s, size_t n, uint32_t *putf32, size_t *nresult)
{
	size_t len, i;
	uint32_t utf32;

	if (n < 1)
		return 1;
	len = utf8len[(unsigned char)s[0]];
	switch (len) {
	case 0:
		return 1;
	case 1:
		utf32 = (unsigned char)s[0];
		break;
	default:
		if (n < len)
			return 1;
		utf32 = s[0] & utf8mask[len];
		for (i = 1; i < len; ++i) {
			if ((s[i] & 0xc0) != 0x80)
				return 1;
			utf32 <<= 6;
			utf32 |= s[i] & 0x3f;
		}
		if (utf32len(utf32) != len)
			return 1;
	}
	*putf32 = utf32;
	*nresult = len;
	return 0;
}


int
escape_utf16(char *s, size_t n, uint16_t utf16, size_t *nresult)
{
	if (utf16 <= 0x7f) {
		if (n < 1)
			return 1;
		s[0] = (unsigned char)utf16;
		*nresult = 1;
	} else {
		if (n < 6)
			return 1;
		s[0] = '\\';
		s[1] = 'u';
		s[2] = utf8_xdigit[(utf16 >> 12) & 0xf];
		s[3] = utf8_xdigit[(utf16 >>  8) & 0xf];
		s[4] = utf8_xdigit[(utf16 >>  4) & 0xf];
		s[5] = utf8_xdigit[ utf16        & 0xf];
		*nresult = 6;
	}
	return 0;
}

int
escape_utf32(char *s, size_t n, uint32_t utf32, size_t *nresult)
{
	uint16_t hi, lo;
	size_t len, tmp;

	if (utf32 <= UTF16_MAX) {
		if (escape_utf16(s, n, (uint16_t)utf32, &len))
			return 1;
	} else if (utf32 <= UTF32_MAX) {
		utf32to16(&hi, &lo, utf32);
		if (escape_utf16(s, n, hi, &len))
			return 1;
		s += len;
		n -= len;
		if (escape_utf16(s, n, lo, &tmp))
			return 1;
		len += tmp;
	} else {
		return 1;
	}
	*nresult = len;
	return 0;
}
