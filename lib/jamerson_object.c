/*-
 * Copyright (c) 2014, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <stdlib.h>
#include <string.h>

#include "jamerson_local.h"

#define JAMERSON_OBJECT_DEFAULT_CAPACITY 32
#define JAMERSON_OBJECT_MAX_CAPACITY (SIZE_MAX / sizeof(char *))

static inline uint32_t
jamerson_rb_hash(const char *s)
{
	uint32_t hash, tmp;

	hash = 0;
	while (*s) {
		hash <<= 4;
		hash += (unsigned char)*s++;
		tmp = hash & 0xf0000000;
		if (tmp != 0) {
			hash ^= tmp;
			hash ^= tmp >> 24;
		}
	}
	return hash;
}

static inline int
jamerson_rb_compare_nodes(const struct jamerson_rb_node *a,
    const struct jamerson_rb_node *b)
{
	return (int)(a->hash - b->hash);
}

RB_PROTOTYPE_STATIC(jamerson_rb_tree, \
    jamerson_rb_node, rbn, jamerson_rb_compare_nodes)
RB_GENERATE_STATIC(jamerson_rb_tree, \
    jamerson_rb_node, rbn, jamerson_rb_compare_nodes)

VECTOR_PROTOTYPE_STATIC(jamerson_keys,
    const char *)
VECTOR_GENERATE_STATIC(jamerson_keys,
    const char *, JAMERSON_OBJECT_DEFAULT_CAPACITY)

jamerson_object_t
jamerson_object_new(void)
{
	jamerson_object_t p;

	p = malloc(sizeof(*p));
	if (p != NULL) {
		RB_INIT(&p->rbt);
		p->keys = VECTOR_NEW(jamerson_keys, NULL);
		if (p->keys != NULL)
			return p;
		free(p);
	}
	return NULL;
}

void
jamerson_object_delete(jamerson_object_t p)
{
	struct jamerson_rb_node *node, *next;

	for (node = RB_MIN(jamerson_rb_tree, &p->rbt);
	    (node != NULL) &&
	    (next = RB_NEXT(jamerson_rb_tree, &p->rbt, node), 1);
	    node = next) {
		RB_REMOVE(jamerson_rb_tree, &p->rbt, node);
		free(node);
	}
	VECTOR_DELETE(jamerson_keys, p->keys);
	free(p);
}

jamerson_value_t
jamerson_object_get(jamerson_object_t p, const char *key)
{
	struct jamerson_rb_node *node, tmp;

	tmp.hash = jamerson_rb_hash(key);
	node = RB_FIND(jamerson_rb_tree, &p->rbt, &tmp);
	if (node != NULL)
		return node->value;
	return NULL;
}

int
jamerson_object_put(jamerson_object_t p,
    const char *key, jamerson_value_t value, jamerson_value_t *old)
{
	struct jamerson_rb_node *node, tmp;
	size_t len;

	tmp.hash = jamerson_rb_hash(key);
	node = RB_FIND(jamerson_rb_tree, &p->rbt, &tmp);
	if (node != NULL) {
		if (old != NULL)
			*old = node->value;
		node->value = value;
		return 0;
	}
	len = strlen(key) + 1;
	if (SIZE_MAX - sizeof(*node) < len)
		return 1;
	node = malloc(sizeof(*node) + len);
	if (node == NULL)
		return 1;
	memcpy(node->key, key, len);
	node->value = value;
	node->hash = tmp.hash;
	RB_INSERT(jamerson_rb_tree, &p->rbt, node);
	if (VECTOR_PUSH_BACK(jamerson_keys, p->keys,
	    (const char *)&node->key[0]))
		goto fatal;
	if (old != NULL)
		*old = NULL;
	return 0;
fatal:
	free(node);
	return 1;
}

const char * const *
jamerson_object_key_set(jamerson_object_t p, size_t *lenp)
{
	*lenp = VECTOR_SIZE(jamerson_keys, p->keys);
	return VECTOR_DATA(jamerson_keys, p->keys);
}
