/*-
 * Copyright (c)2019, 2021 Takehiko NOZAKI,
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#ifndef UNICODE_H_
#define UNICODE_H_

#include <stddef.h>
#include <stdint.h>

#define UTF16_MAX	UINT16_C(0xffff)
#define UTF32_MAX	UINT32_C(0x10ffff)

/* surrogate pair */
#define SURROGATE_HI_MIN	UINT16_C(0xd800)
#define SURROGATE_HI_MAX	UINT16_C(0xdbff)
#define SURROGATE_LO_MIN	UINT16_C(0xdc00)
#define SURROGATE_LO_MAX	UINT16_C(0xdfff)
#define SURROGATE_BASE		UINT16_C(0x10000)

static inline int
is_hi_surrogate(uint16_t utf16)
{
	return utf16 >= SURROGATE_HI_MIN && utf16 <= SURROGATE_HI_MAX;
}

static inline int
is_lo_surrogate(uint16_t utf16)
{
	return utf16 >= SURROGATE_LO_MIN && utf16 <= SURROGATE_LO_MAX;
}

static inline uint32_t
utf16to32(uint16_t hi, uint16_t lo)
{
	hi -= SURROGATE_HI_MIN;
	lo -= SURROGATE_LO_MIN;
	return ((uint32_t)hi << 10 | (uint32_t)lo) + SURROGATE_BASE;
}

static inline void
utf32to16(uint16_t *hi, uint16_t *lo, uint32_t utf32)
{
	utf32 -= SURROGATE_BASE;
	*hi = (uint16_t)((utf32 >> 10) + SURROGATE_HI_MIN);
	*lo = (uint16_t)((utf32 & UINT32_C(0x3ff)) + SURROGATE_LO_MIN);
}

#define UTF8_BUFSIZ	4

int utf32to8(char *, size_t, uint32_t, size_t *);
int utf8to32(const char *, size_t, uint32_t *, size_t *);

#define UNICODE_ESCAPE_BUFSIZ	16

int escape_utf16(char *, size_t, uint16_t, size_t *);
int escape_utf32(char *, size_t, uint32_t, size_t *);

#define UTF8_CTRL_NUL			(0x00) /* \0 */
#define UTF8_CTRL_SOH			(0x01)
#define UTF8_CTRL_STX			(0x02)
#define UTF8_CTRL_ETX			(0x03)
#define UTF8_CTRL_EOT			(0x04)
#define UTF8_CTRL_ENQ			(0x05)
#define UTF8_CTRL_ACK			(0x06)
#define UTF8_CTRL_BEL			(0x07)
#define UTF8_CTRL_BS			(0x08) /* \b */
#define UTF8_CTRL_HT			(0x09) /* \t */
#define UTF8_CTRL_LF			(0x0a) /* \n */
#define UTF8_CTRL_VT			(0x0b)
#define UTF8_CTRL_FF			(0x0c) /* \f */
#define UTF8_CTRL_CR			(0x0d) /* \r */
#define UTF8_CTRL_SO			(0x0e)
#define UTF8_CTRL_SI			(0x0f)
#define UTF8_CTRL_DLE			(0x10)
#define UTF8_CTRL_DC1			(0x11)
#define UTF8_CTRL_DC2			(0x12)
#define UTF8_CTRL_DC3			(0x13)
#define UTF8_CTRL_DC4			(0x14)
#define UTF8_CTRL_NAK			(0x15)
#define UTF8_CTRL_SYN			(0x16)
#define UTF8_CTRL_ETB			(0x17)
#define UTF8_CTRL_CAN			(0x18)
#define UTF8_CTRL_EM			(0x19)
#define UTF8_CTRL_SUB			(0x1a)
#define UTF8_CTRL_ESC			(0x1b)
#define UTF8_CTRL_FS			(0x1c)
#define UTF8_CTRL_GS			(0x1d)
#define UTF8_CTRL_RS			(0x1e)
#define UTF8_CTRL_US			(0x1f)
#define UTF8_SPACE			(0x20) /*    */
#define UTF8_QUOTATION_MARK		(0x22) /* "  */
#define UTF8_PLUS_SIGN			(0x2b) /* +  */
#define UTF8_COMMA			(0x2c) /* ,  */
#define UTF8_HYPHEN_MINUS		(0x2d) /* -  */
#define UTF8_FULL_STOP			(0x2e) /* .  */
#define UTF8_SOLIDUS			(0x2f) /* /  */
#define UTF8_DIGIT_ZERO			(0x30) /* 0  */
#define UTF8_DIGIT_ONE			(0x31) /* 1  */
#define UTF8_DIGIT_TWO			(0x32) /* 2  */
#define UTF8_DIGIT_THREE		(0x33) /* 3  */
#define UTF8_DIGIT_FOUR			(0x34) /* 4  */
#define UTF8_DIGIT_FIVE			(0x35) /* 5  */
#define UTF8_DIGIT_SIX			(0x36) /* 6  */
#define UTF8_DIGIT_SEVEN		(0x37) /* 7  */
#define UTF8_DIGIT_EIGHT		(0x38) /* 8  */
#define UTF8_DIGIT_NINE			(0x39) /* 9  */
#define UTF8_COLON			(0x3a) /* :  */
#define UTF8_LATIN_CAPITAL_A		(0x41) /* A  */
#define UTF8_LATIN_CAPITAL_B		(0x42) /* B  */
#define UTF8_LATIN_CAPITAL_C		(0x43) /* C  */
#define UTF8_LATIN_CAPITAL_D		(0x44) /* D  */
#define UTF8_LATIN_CAPITAL_E		(0x45) /* E  */
#define UTF8_LATIN_CAPITAL_F		(0x46) /* F  */
#define UTF8_LATIN_CAPITAL_G		(0x47) /* G  */
#define UTF8_LATIN_CAPITAL_H		(0x48) /* H  */
#define UTF8_LATIN_CAPITAL_I		(0x49) /* I  */
#define UTF8_LATIN_CAPITAL_J		(0x4a) /* J  */
#define UTF8_LATIN_CAPITAL_K		(0x4b) /* K  */
#define UTF8_LATIN_CAPITAL_L		(0x4c) /* L  */
#define UTF8_LATIN_CAPITAL_M		(0x4d) /* M  */
#define UTF8_LATIN_CAPITAL_N		(0x4e) /* N  */
#define UTF8_LATIN_CAPITAL_O		(0x4f) /* O  */
#define UTF8_LATIN_CAPITAL_P		(0x50) /* P  */
#define UTF8_LATIN_CAPITAL_Q		(0x51) /* Q  */
#define UTF8_LATIN_CAPITAL_R		(0x52) /* R  */
#define UTF8_LATIN_CAPITAL_S		(0x53) /* S  */
#define UTF8_LATIN_CAPITAL_T		(0x54) /* T  */
#define UTF8_LATIN_CAPITAL_U		(0x55) /* U  */
#define UTF8_LATIN_CAPITAL_V		(0x56) /* V  */
#define UTF8_LATIN_CAPITAL_W		(0x57) /* W  */
#define UTF8_LATIN_CAPITAL_X		(0x58) /* X  */
#define UTF8_LATIN_CAPITAL_Y		(0x59) /* Y  */
#define UTF8_LATIN_CAPITAL_Z		(0x5a) /* Z  */
#define UTF8_LEFT_SQUARE_BRACKET	(0x5b) /* [  */
#define UTF8_REVERSE_SOLIDUS		(0x5c) /* \  */
#define UTF8_RIGHT_SQUARE_BRACKET	(0x5d) /* ]  */
#define UTF8_LATIN_SMALL_A		(0x61) /* a  */
#define UTF8_LATIN_SMALL_B		(0x62) /* b  */
#define UTF8_LATIN_SMALL_C		(0x63) /* c  */
#define UTF8_LATIN_SMALL_D		(0x64) /* d  */
#define UTF8_LATIN_SMALL_E		(0x65) /* e  */
#define UTF8_LATIN_SMALL_F		(0x66) /* f  */
#define UTF8_LATIN_SMALL_G		(0x67) /* g  */
#define UTF8_LATIN_SMALL_H		(0x68) /* h  */
#define UTF8_LATIN_SMALL_I		(0x69) /* i  */
#define UTF8_LATIN_SMALL_J		(0x6a) /* j  */
#define UTF8_LATIN_SMALL_K		(0x6b) /* k  */
#define UTF8_LATIN_SMALL_L		(0x6c) /* l  */
#define UTF8_LATIN_SMALL_M		(0x6d) /* m  */
#define UTF8_LATIN_SMALL_N		(0x6e) /* n  */
#define UTF8_LATIN_SMALL_O		(0x6f) /* o  */
#define UTF8_LATIN_SMALL_P		(0x70) /* p  */
#define UTF8_LATIN_SMALL_Q		(0x71) /* q  */
#define UTF8_LATIN_SMALL_R		(0x72) /* r  */
#define UTF8_LATIN_SMALL_S		(0x73) /* s  */
#define UTF8_LATIN_SMALL_T		(0x74) /* t  */
#define UTF8_LATIN_SMALL_U		(0x75) /* u  */
#define UTF8_LATIN_SMALL_V		(0x76) /* v  */
#define UTF8_LATIN_SMALL_W		(0x77) /* w  */
#define UTF8_LATIN_SMALL_X		(0x78) /* x  */
#define UTF8_LATIN_SMALL_Y		(0x79) /* y  */
#define UTF8_LATIN_SMALL_Z		(0x7a) /* z  */
#define UTF8_LEFT_CURLY_BRACKET		(0x7b) /* {  */
#define UTF8_RIGHT_CURLY_BRACKET	(0x7d) /* }  */

extern const unsigned char utf8tab[];

static inline int
utf8_isspace(int c)
{
	return utf8tab[c] & 0x80;
}

static inline int
utf8_digittoint(int c)
{
	return (int)utf8tab[c] & ~0x80;
}

static inline int
utf8_inttodigit(int digit)
{
	return UTF8_DIGIT_ZERO + digit;
}

#include <stdlib.h>
#define utf8_strtod	strtod

#endif
