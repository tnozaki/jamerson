# libJamerSON - yet another JSON parser for C

## Build prequisite
- ISO/IEC 9899:1999 + IEEE Std 1003.1-2008 environment
- [libtool](https://www.gnu.org/software/libtool/)  >=2.4.6
- [autoconf](https://www.gnu.org/software/autoconf/autoconf.html) >=2.71
- [automake](https://www.gnu.org/software/automake/) >=2.16.3
- [algorithm](https://bitbucket.org/tnozaki/algorithm/) >=0.0.0

## How to install
```sh
$ git clone -b master https://tnozaki@bitbucket.org/tnozaki/jamerson.git
$ cd jamerson
$ ./autogen.sh
$ ./configure
$ make && make check && sudo make install
$ make distclean
```

## How to use

### using event driven parser(XML's SAX like)
```c
#include <stdlib.h>
#include <jamerson_parser.h>

struct my_context {
	...
};

static int
my_event_handler(void *ctx, jamerson_event_t event, ...)
{
	va_list ap;
	int ret;
	struct my_context *myctx;
	bool b;
	double d;
	const char *key;
	const char *s;

	myctx = (struct my_context *)ctx;
	va_start(ap, event);
	switch (event) {
	/* begin array */
	case BEGIN_ARRAY:
		...
		break;
	/* end array */
	case END_ARRAY:
		...
		break;
	/* boolean */
	case VALUE_BOOLEAN:
		b = (bool)va_arg(ap, int);
		...
		break;
	/* null */
	case VALUE_NULL:
		...
		break;
	/* number */
	case VALUE_NUMBER:
		d = (double)va_arg(ap, double);
		...
		break;
	/* begin object */
	case BEGIN_OBJECT:
		...
		break;
	/* object key */
	case KEY_NAME:
		key = (const char *)va_arg(ap, const char *);
		...
		break;
	/* end object */
	case END_OBJECT:
		...
		break;
	/* string */
	case VALUE_STRING:
		s = (const char *)va_arg(ap, const char *);
		...
		break;
	default:
		return 1;
	}
	va_end(ap);
	return 0;
}

int
main(int argc, char *argv[])
{
	FILE *fp;
	struct my_context ctx;

	/* stdin, fopen, fmemopen/open_memstream as you like */
	fp = ...
	if (jamerson_parse_from_file(fp,
	    &my_event_handler, (void*)&ctx))
		abort();
	exit(EXIT_SUCCESS);
}
```
### using document object model parser(XML's DOM like)
```c
#include <stdlib.h>
#include <jamerson_load.h>

int
main(int argc, char *argv[])
{
	FILE *fp;
	jamerson_value_t root, jvalue;
	jamerson_array_t jarray;
	jamerson_boolean_t jboolean;
	jamerson_null_t jnull;
	jamerson_number_t jnumber;
	jamerson_object_t jobject;
	jamerson_string_t jstring;

	/* stdin, fopen, fmemopen/open_memstream as you like */
	fp = ...
	root = jamerson_load_file(fp);
	if (root == NULL)
		abort();
	if ((jarray = jamerson_value_is_array(root)) != NULL) {
		size_t len, i;
		len = jamerson_array_size(jarray);
		for (i = 0; i < len; ++i) {
			jvalue = jamerson_array_get(jarray, i);
			...
		}
	} else if ((jboolean = jamerson_value_is_boolean(root)) != NULL) {
		bool b;
		b = jamerson_boolean_boolean_value(jboolean);
		...
	} else if ((jnull = jamerson_value_is_null(root)) != NULL) {
		...
	} else if ((jnumber = jamerson_value_is_number(root)) != NULL) {
		double d;
		d = jamerson_number_double_value(jnumber);
		...
	} else if ((jobject = jamerson_value_is_object(root)) != NULL) {
		const char * const *keys;
		size_t len, i;
		keys = jamerson_object_key_set(jobject, &len);
		for (i = 0; i < len; ++i) {
			jvalue = jamerson_object_get(jobject, keys[i]);
			...
		}
	} else if ((jstring = jamerson_value_is_string(root)) != NULL) {
		const char *string;
		string = jamerson_string_string_value(jstring);
		...
	} else {
		abort();
	}
	jamerson_value_delete(root);
	exit(EXIT_SUCCESS);
}
```
### using path/query driven parser(XML's XPath like)

TBD

### building document object tree and encode to JSON format
```c
#include <stdlib.h>
#include <jamerson_store.h>

int
main(void)
{
	FILE *fp;
	jamerson_value_t jvalue;
	jamerson_array_t jarray;
	jamerson_boolean_t jboolean;
	jamerson_null_t jnull;
	jamerson_number_t jnumber;
	jamerson_object_t root, jobject;
	jamerson_string_t jstring;

	/* root is object */
	root = jamerson_object_new();
	if (root == NULL)
		abort();
	/* array */
	jarray = jamerson_array_new();
	if (jarray == NULL)
		abort();
	jvalue = jamerson_value_new_array(jarray);
	if (jvalue == NULL)
		abort();
	if (jamerson_object_put(root, "jamerson_array", jvalue, NULL))
		abort();
	/* boolean */
	jboolean = jamerson_boolean_new(false);
	jvalue = jamerson_value_new_boolean(jboolean);
	if (jvalue == NULL)
		abort();
	if (jamerson_object_put(root, "jamerson_boolean", jvalue, NULL))
		abort();
	/* null */
	jnull = jamerson_null_new();
	jvalue = jamerson_value_new_null(jnull);
	if (jvalue == NULL)
		abort();
	if (jamerson_object_put(root, "jamerson_null", jvalue, NULL))
		abort();
	/* number */
	jnumber = jamerson_number_new(0.0);
	jvalue = jamerson_value_new_number(jnumber);
	if (jvalue == NULL)
		abort();
	if (jamerson_object_put(root, "jamerson_number", jvalue, NULL))
		abort();
	/* object */
	jobject = jamerson_object_new();
	if (jobject == NULL)
		abort();
	jvalue = jamerson_value_new_object(jobject);
	if (jvalue == NULL)
		abort();
	if (jamerson_object_put(root, "jamerson_object", jvalue, NULL))
		abort();
	/* string */
	jstring = jamerson_string_new("");
	jvalue = jamerson_value_new_string(jstring);
	if (jvalue == NULL)
		abort();
	if (jamerson_object_put(root, "jamerson_string", jvalue, NULL))
		abort();
	/* convert root to jamerson_value_t */
	jvalue = jamerson_value_new_object(root);
	if (jvalue == NULL)
		abort();
	/* stdout, fopen, fmemopen/open_memstream as you like */
	fp = ...
	if (jamerson_store_file(jvalue, fp,
	    JAMERSON_CANONICAL|
	    JAMERSON_PRETTY_PRINT|
	    JAMERSON_UNICODE_ESCAPE))
		abort();
	jamerson_value_delete(jvalue);
	exit(EXIT_SUCCESS);
}
```

## License
2-clause BSDL
```text
Copyright (c) 2014, 2021 Takehiko NOZAKI,
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
```
